# RealWorldOne Challenge

## NOTE:

Currently we use server to relay messages with another user. This is done to save time. In an ideal scenario, there would be a session created and a room created in centrifugo and everything would be published there and it would act as the broker and the server would no longer act as a relay

Please find the demo here:

https://realworldonechallenge.herokuapp.com