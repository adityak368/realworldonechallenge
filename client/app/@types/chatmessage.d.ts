interface ChatMsg {
    timestamp: Date;
    text: string;
    from: string;
    to: string;
}
