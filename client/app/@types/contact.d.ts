interface IContact {
    id: string;
    email: string;
    authToken: string;
    name: string;
    username: string;
    msg: Array<ChatMsg>;
}
