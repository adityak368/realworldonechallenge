import AuthManager from 'common/authmanager';
import ContactManager from 'common/contactmanager';
import Broker from 'lib/comm/messaging/broker';

declare global {
    interface Window {
        broker: Broker;
        authManager: AuthManager;
        contactManager: ContactManager;
    }
}
