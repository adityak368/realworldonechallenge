interface IMediaDevice {
    stream: MediaStream | null,
    deviceId: string,
    label: string,
    groupId: string,
    isEnabled: boolean;
}
