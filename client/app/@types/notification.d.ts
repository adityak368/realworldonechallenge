interface INotification {
    text: string;
    title: string;
    date: Date;
    data?: any;
}
