interface IUser {
    id: string;
    email: string;
    authToken: string;
    name: string;
    username: string;
}
