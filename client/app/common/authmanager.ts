import { Request } from 'lib/net/request';
import config from 'app/config';

const { HOST } = config;

export default class AuthManager {
    iUpdateProfile = (formData: any) => {
        const req: Request = new Request(`${HOST}/auth/profile`);
        return req.put(formData);
    };

    iLogin = (formData: any) => {
        const req: Request = new Request(`${HOST}/auth/login`);
        return req.post(formData);
    };

    iSignUp = (formData: any) => {
        const req: Request = new Request(`${HOST}/auth/signup`);
        return req.post(formData);
    };

    iChangePassword = (formData: any) => {
        const req: Request = new Request(`${HOST}/auth/changepassword`);
        return req.post(formData);
    };

    iForgotPassword = (formData: any) => {
        const req: Request = new Request(`${HOST}/auth/forgotpassword`);
        return req.post(formData);
    };

    iResetPassword = (formData: any) => {
        const req: Request = new Request(`${HOST}/auth/resetpassword`);
        return req.post(formData);
    };
}
