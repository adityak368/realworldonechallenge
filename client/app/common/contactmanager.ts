import { Request } from 'lib/net/request';
import config from 'app/config';
import Peer from 'simple-peer';
import { BrokerSignals, GenerateBrokerSignal } from 'lib/comm/messaging/types';

const { HOST, API } = config;

export default class ContactManager {
    iRelay = (target: string, data: any) => {
        const req: Request = new Request(`${HOST}${API}/relay`);
        return req.post({
            target,
            data
        });
    };

    iFetchContacts = () => {
        const req: Request = new Request(`${HOST}${API}/contacts`);
        return req.get();
    };

    iBroadCastAudioVideoState = (
        connections: {
            [peerId: number]: Peer;
        },
        username: number,
        streamID: string,
        isEnabled: boolean
    ) => {
        Object.keys(connections).forEach(key => {
            const connection = connections[key];
            if (connection._channel.readyState === 'open') {
                connection.send(
                    JSON.stringify(
                        GenerateBrokerSignal(
                            BrokerSignals.UpdateAudioVideoState,
                            {
                                streamID,
                                username,
                                isEnabled
                            }
                        )
                    )
                );
            }
        });
    };

    iSendChatMessage = (sessionId: string, message: any) => {
        const req: Request = new Request(
            `${HOST}${API}/session/${sessionId}/chat`
        );
        return req.post({
            message
        });
    };

    iSendMessageToPeer = (
        peerSignal: BrokerSignals,
        connection: Peer,
        data: any
    ) => {
        if (connection && connection._channel.readyState === 'open') {
            connection.send(
                JSON.stringify(GenerateBrokerSignal(peerSignal, data))
            );
        }
    };
}
