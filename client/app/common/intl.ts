interface Options {
    locales: {
        [key: string]: {
            [key: string]: string;
        };
    };
    currentLocale?: string;
}

class Intl {
    private options: Options;

    init(options: Options) {
        this.options = options;
        this.options.currentLocale =
            window.localStorage.getItem('locale') ||
            options.currentLocale ||
            'en-US';
    }

    get(key: string) {
        const { locales, currentLocale } = this.options;
        if (locales.hasOwnProperty(currentLocale)) {
            return (
                locales[currentLocale][key] ||
                locales[currentLocale]['en-US'] ||
                ''
            );
        }
        return '';
    }

    getf(key: string, ...rest: Array<string>) {
        const { locales, currentLocale } = this.options;
        if (locales.hasOwnProperty(currentLocale)) {
            try {
                let transString =
                    locales[currentLocale][key] ||
                    locales[currentLocale]['en-US'] ||
                    '';
                if (transString) {
                    rest.forEach((val, index) => {
                        transString = transString.replace(`{${index}}`, val);
                    });
                    return transString;
                }
            } catch (err) {}
        }
        return '';
    }

    setLocale(locale: string) {
        this.options.currentLocale = locale;
        window.localStorage.setItem('locale', locale);
        window.location.reload();
    }

    getCurrentLocale() {
        return this.options.currentLocale;
    }
}

const intl = new Intl();

export default intl;
