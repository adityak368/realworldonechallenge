class Logger {

    info = msg => {
        console.info(msg);
    }

    error = msg => {
        console.error(msg);
    }

    warn = msg => {
        console.info(msg);
    }
}

const logger = new Logger();

export default logger;
