import { notification } from 'antd';

class Notification {

    success = (options: any) => {
        notification.success(options);
    }

    info = (options: any) => {
        notification.info(options);
    }

    warning = (options: any) => {
        notification.warning(options);
    }

    error = (options: any) => {
        notification.error(options);
    }
}

const notificationObj: Notification = new Notification();

export default notificationObj;
