export enum Dialogs {}

export const LANGUAGES = [
    {
        key: 'en-US',
        title: 'English',
        flag: '/res/img/america.svg'
    }
];

export enum MediaType {
    Audio = 'audio',
    Video = 'video',
    Speaker = 'speaker'
}
