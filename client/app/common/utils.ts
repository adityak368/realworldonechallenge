import * as jwt from 'jsonwebtoken';
import { Dialogs } from 'common/types';

export const decodeToken = (authToken: string): IUser => {
    try {
        const tokenData: any = jwt.decode(authToken).info;
        return {
            ...tokenData,
            authToken
        };
    } catch (err) {
        return null;
    }
};

export const openDialog = (dialog: Dialogs, caller: any) => {
    const { dialogs } = caller.state;
    dialogs[dialog] = true;
    caller.setState({
        dialogs
    });
};

export const closeDialog = (dialog: Dialogs, caller: any) => {
    const { dialogs } = caller.state;
    dialogs[dialog] = false;
    caller.setState({
        dialogs
    });
};

export const capitalize = (str: string): string =>
    str
        .toString()
        .charAt(0)
        .toUpperCase() + str.toString().slice(1);

export const getName = (user: IUser) => {
    if (!user) {
        return '';
    }
    return user.username;
};

export const getUserEmail = (user: IUser) => {
    if (!user) {
        return '';
    }
    return user.email;
};
