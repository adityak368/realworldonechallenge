import React from 'react';
import { Upload, Icon } from 'antd';
import intl from 'common/intl';
import notification from 'common/notification';

interface State {
    loading: boolean;
    imageUrl: string;
}

export default class Avatar extends React.Component<{}, State> {
    state = {
        loading: false,
        imageUrl: ''
    };

    beforeUpload = file => {
        const isJpgOrPng =
            file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            notification.error({ message: intl.get('ErrorPhotoFormat') });
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            notification.error({ message: intl.get('ErrorPhotoSize') });
        }
        return isJpgOrPng && isLt2M;
    };

    getBase64 = (img, callback) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    };

    handleChange = info => {
        if (info.file.status === 'uploading') {
            this.setState({ loading: true });
            return;
        }
        if (info.file.status === 'done') {
            // Get this url from response in real world.
            this.getBase64(info.file.originFileObj, imageUrl =>
                this.setState({
                    imageUrl,
                    loading: false
                })
            );
        }
    };

    render() {
        const { imageUrl } = this.state;
        return (
            <Upload
                accept="image/gif, image/jpeg, image/png"
                name="avatar"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                action={'/'}
                beforeUpload={this.beforeUpload}
                onChange={this.handleChange}
            >
                {imageUrl ? (
                    <img
                        src={imageUrl}
                        alt="avatar"
                        style={{ width: '100%' }}
                    />
                ) : (
                    <div>
                        <Icon type={this.state.loading ? 'loading' : 'plus'} />
                        <div className="ant-upload-text">
                            {intl.get('ProfilePhoto')}
                        </div>
                    </div>
                )}
            </Upload>
        );
    }
}
