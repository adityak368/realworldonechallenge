import React from 'react';
import Logger from 'common/logger';
import ErrorPage from 'pages/ErrorPage';

interface State {
    hasError: boolean;
}

export default class ErrorBoundary extends React.Component<{}, State> {
    state = {
        hasError: false
    };

    componentDidCatch(error, info) {
        // Display fallback UI
        this.setState({ hasError: true });
        Logger.error({ error, info });
    }

    retry = () => {
        this.setState({
            hasError: false
        });
    };

    render() {
        const { hasError } = this.state;
        if (hasError) {
            return <ErrorPage retry={this.retry} />;
        }
        return this.props.children;
    }
}
