import React from 'react';
import classNames from 'classnames';

import './Loader.less';

interface Props {
    spinning: boolean;
    fullScreen?: boolean;
}

const Loader: React.FC<Props> = ({ spinning = false, fullScreen }) => (
    <div
        className={classNames('loader', {
            ['hidden']: !spinning,
            ['fullScreen']: fullScreen,
        })}
    >
        <div className="wrapper">
            <div className="inner" />
            <div className="text">LOADING</div>
        </div>
    </div>
)

export default Loader;
