import React from 'react';
import { Link } from 'react-router-dom';

const Logo = ({ isSideBarOpen = true, ...rest }) => (
    <div {...rest} id="logo">
        <Link to={'/'}>
            <img
                style={{ width: '100%', height: '100%' }}
                src={
                    !isSideBarOpen
                        ? '/res/img/favicon.png'
                        : '/res/img/logo.png'
                }
            />
        </Link>
    </div>
);

export default Logo;
