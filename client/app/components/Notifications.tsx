import React from 'react';
import intl from 'common/intl';
import { Popover, List, Badge, Icon, Button } from 'antd';
import moment from 'moment';
import withStyles, { WithStyles } from 'react-jss';

const styles = theme => ({
    notificationIcon: {
        fontSize: theme.fontSize,
        cursor: 'pointer',
        transition: 'color 0.3s',
        padding: '0 24px',
        height: '100%',
        backgroundColor: 'transparent',
        border: 'none',
        '&:focus': { outline: 0, color: theme.hoverColor }
    },
    listItem: {
        cursor: 'pointer',
        '&:hover': { backgroundColor: '#FAFAFA' }
    }
});

interface Props {
    onClearNotifications: () => void;
    notifications: Array<INotification>;
}

type StyledProps = WithStyles<typeof styles> & Props;

const Notifications: React.FC<StyledProps> = ({
    notifications,
    classes,
    onClearNotifications
}) => (
    <Popover
        placement="bottom"
        trigger="click"
        key="notifications"
        getPopupContainer={() => document.querySelector('#header')}
        content={
            <React.Fragment>
                <List
                    itemLayout="horizontal"
                    dataSource={notifications}
                    renderItem={(item: INotification) => (
                        <List.Item
                            onClick={() => {}}
                            className={classes.listItem}
                        >
                            <List.Item.Meta
                                title={item.title}
                                description={moment(item.date).fromNow()}
                            />
                            <Icon
                                style={{
                                    fontSize: 10,
                                    color: '#ccc'
                                }}
                                type="right"
                                theme="outlined"
                            />
                        </List.Item>
                    )}
                />
                {notifications.length ? (
                    <Button
                        style={{ width: '100%' }}
                        onClick={onClearNotifications}
                    >
                        {intl.get('ClearNotifications')}
                    </Button>
                ) : null}
            </React.Fragment>
        }
    >
        <Badge count={notifications.length} offset={[-20, 20]}>
            <button className={classes.notificationIcon}>
                <Icon type="bell" />
            </button>
        </Badge>
    </Popover>
);

export default withStyles(styles)(Notifications);
