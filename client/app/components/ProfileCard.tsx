import React from 'react';

import styles from 'styles/Profile';
import withStyles, { WithStyles } from 'react-jss';

import { Typography, Card, Icon } from 'antd';
import AvatarUpload from 'components/AvatarUpload';
import { getName, getUserEmail } from 'common/utils';

const { Text, Title } = Typography;

interface Props {
    user: IUser;
}
type StyledProps = WithStyles<typeof styles> & Props;

const ProfileCard: React.FC<StyledProps> = ({ user, classes }) => (
    <React.Fragment>
        <Card>
            <div className={classes.container}>
                <AvatarUpload />
                <Title level={4}>{getName(user)}</Title>
                <div className={classes.profileItem}>
                    <Icon type="mail" className={classes.icon} />
                    <Text type="secondary">{getUserEmail(user)}</Text>
                </div>
                <div className={classes.profileItem}>
                    <Icon type="phone" className={classes.icon} />
                    <Text type="secondary">{user.Contact}</Text>
                </div>
            </div>
        </Card>
    </React.Fragment>
);

export default withStyles(styles)(ProfileCard);
