import React from 'react';
import withStyles, { WithStyles } from 'react-jss';

import userPlaceHolder from 'res/img/user-placeholder.png';

import { MediaType } from 'common/types';
import { List, Tooltip } from 'antd';
import { IAuthState } from 'data/reducers/auth';
import { IContactState } from 'data/reducers/contacts';
import { When } from 'react-if';

const styles = theme => ({
    thumbVideo: {
        width: '100%',
        height: '100%',
        objectFit: 'cover'
    },
    participant: {
        cursor: 'pointer',
        borderRadius: '3px',
        height: 120,
        width: 120
    },
    userdisplay: {
        width: '100%',
        display: 'flex',
        margin: 10,
        height: 120,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        flexWrap: 'wrap',
        overflow: 'hidden',
        '&:hover': {
            overflow: 'auto'
        }
    }
});

interface Props extends WithStyles<typeof styles> {
    selectedContact: string;
    getPeerStreamOfType: (peerID: number, mediaType: MediaType) => MediaStream;
    contacts: IContactState;
}

type StyledProps = WithStyles<typeof styles> & Props;

const UserDisplay: React.FC<StyledProps> = ({
    classes,
    selectedContact,
    getPeerStreamOfType,
    contacts
}: StyledProps) => {
    const hasStream =
        contacts.streams[selectedContact] &&
        contacts.streams[selectedContact].some(s => s.isEnabled);
    return (
        <When condition={hasStream === true}>
            <div className={classes.userdisplay}>
                <List
                    itemLayout="vertical"
                    dataSource={[
                        {
                            peerID: selectedContact,
                            title: selectedContact
                        }
                    ]}
                    renderItem={({ peerID, title }) => {
                        const videoStream = getPeerStreamOfType(
                            peerID,
                            MediaType.Video
                        );
                        const audioStream = getPeerStreamOfType(
                            peerID,
                            MediaType.Audio
                        );
                        return (
                            <List.Item
                                style={{
                                    padding: 0,
                                    border: '2px solid #dedede'
                                }}
                                className={classes.participant}
                            >
                                <Tooltip title={title}>
                                    <video
                                        key={
                                            videoStream
                                                ? videoStream.id
                                                : `videoThumb${peerID}`
                                        }
                                        autoPlay
                                        muted
                                        ref={video => {
                                            if (video && videoStream) {
                                                const videoTracks = videoStream.getVideoTracks();
                                                const isAnyVideoTrackEnabled = videoTracks.some(
                                                    track =>
                                                        track.enabled === true
                                                );
                                                if (isAnyVideoTrackEnabled) {
                                                    video.srcObject = videoStream;
                                                } else {
                                                    video.srcObject = null;
                                                }
                                            }
                                        }}
                                        poster={userPlaceHolder}
                                        className={classes.thumbVideo}
                                    />
                                    <audio
                                        id={`audioThumb${peerID}`}
                                        key={
                                            audioStream
                                                ? audioStream.id
                                                : `audioThumb${peerID}`
                                        }
                                        autoPlay
                                        ref={(audio: any) => {
                                            if (audio && audioStream) {
                                                const audioTracks = audioStream.getAudioTracks();
                                                const isAnyAudioTrackEnabled = audioTracks.some(
                                                    track =>
                                                        track.enabled === true
                                                );
                                                if (isAnyAudioTrackEnabled) {
                                                    audio.srcObject = audioStream;
                                                } else {
                                                    audio.srcObject = null;
                                                }
                                            }
                                        }}
                                    />
                                </Tooltip>
                            </List.Item>
                        );
                    }}
                />
            </div>
        </When>
    );
};

export default withStyles(styles)(UserDisplay);
