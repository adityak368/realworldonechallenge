import React from 'react';
import intl from 'common/intl';

import {
    DatePicker,
    Form,
    Input,
    TimePicker,
    Select,
    Tag,
    Tooltip,
    Icon,
    Radio,
    Upload,
    Button
} from 'antd';

import notification from 'common/notification';

const FormItem = Form.Item;
const { Option } = Select;
const { TextArea } = Input;

const CreateAntField = AntComponent => ({
    field,
    form,
    hasFeedback,
    label,
    selectOptions,
    radioOptions,
    submitCount,
    type,
    formWrapperClassName,
    formItemProps,
    ...props
}) => {
    const touched = form.touched[field.name];
    const submitted = submitCount > 0;
    const hasError = form.errors[field.name];
    const submittedError = hasError && submitted;
    const touchedError = hasError && touched;
    const onInputChange = ({ target: { value } }) =>
        form.setFieldValue(field.name, value);
    const onChange = value => form.setFieldValue(field.name, value);
    const onBlur = () => form.setFieldTouched(field.name, true);
    return (
        <FormItem
            className={formWrapperClassName}
            label={label}
            hasFeedback={
                (hasFeedback && submitted) || (hasFeedback && touched)
                    ? true
                    : false
            }
            help={submittedError || touchedError ? hasError : false}
            validateStatus={
                submittedError || touchedError ? 'error' : 'success'
            }
            {...formItemProps}
        >
            <AntComponent
                {...field}
                {...props}
                type={type}
                onBlur={onBlur}
                onChange={type ? onInputChange : onChange}
            >
                {selectOptions &&
                    selectOptions.map(option => (
                        <Option value={option.key} key={option.key}>
                            {option.value}
                        </Option>
                    ))}
            </AntComponent>
        </FormItem>
    );
};

const CreateAntRadioField = AntComponent => ({
    field,
    form,
    hasFeedback,
    label,
    radioOptions,
    submitCount,
    type,
    ...props
}) => {
    const touched = form.touched[field.name];
    const submitted = submitCount > 0;
    const hasError = form.errors[field.name];
    const submittedError = hasError && submitted;
    const touchedError = hasError && touched;
    const onInputChange = ({ target: { value } }) =>
        form.setFieldValue(field.name, value);
    const onChange = value => form.setFieldValue(field.name, value);
    const onBlur = () => form.setFieldTouched(field.name, true);
    return (
        <div className="field-container">
            <FormItem
                label={label}
                hasFeedback={
                    (hasFeedback && submitted) || (hasFeedback && touched)
                        ? true
                        : false
                }
                help={submittedError || touchedError ? hasError : false}
                validateStatus={
                    submittedError || touchedError ? 'error' : 'success'
                }
            >
                <AntComponent
                    {...field}
                    {...props}
                    onBlur={onBlur}
                    onChange={type ? onInputChange : onChange}
                >
                    {radioOptions &&
                        radioOptions.map(option => (
                            <Radio.Button value={option.key} key={option.key}>
                                {option.value}
                            </Radio.Button>
                        ))}
                </AntComponent>
            </FormItem>
        </div>
    );
};

export class AntInputWithTags extends React.Component<
    any,
    {
        inputVisible: boolean;
        inputValue: string;
    }
> {
    state = {
        inputVisible: false,
        inputValue: ''
    };

    input = null;

    handleClose = removedTag => {
        const { field, form } = this.props;
        const tags = field.value.filter(tag => tag !== removedTag);
        form.setFieldValue(field.name, tags);
    };

    showInput = () => {
        this.setState({ inputVisible: true }, () => this.input.focus());
    };

    handleInputChange = e => {
        this.setState({ inputValue: e.target.value });
    };

    handleInputConfirm = () => {
        const { inputValue } = this.state;
        const { field, form } = this.props;
        let tags = field.value;
        if (inputValue && tags.indexOf(inputValue) === -1) {
            tags = [...tags, inputValue];
        }
        this.setState({
            inputVisible: false,
            inputValue: ''
        });
        form.setFieldValue(field.name, tags);
    };

    saveInputRef = input => (this.input = input);

    render() {
        const { inputVisible, inputValue } = this.state;
        const { field, placeholder } = this.props;
        return (
            <div style={{ textAlign: 'left' }}>
                {field.value.map((tag, index) => {
                    const isLongTag = tag.length > 20;
                    const tagElem = (
                        <Tag
                            key={tag}
                            closable
                            onClose={() => this.handleClose(tag)}
                        >
                            {isLongTag ? `${tag.slice(0, 20)}...` : tag}
                        </Tag>
                    );
                    return isLongTag ? (
                        <Tooltip title={tag} key={tag}>
                            {tagElem}
                        </Tooltip>
                    ) : (
                        tagElem
                    );
                })}
                {inputVisible && (
                    <Input
                        ref={this.saveInputRef}
                        style={{ width: 78 }}
                        value={inputValue}
                        onChange={this.handleInputChange}
                        onBlur={this.handleInputConfirm}
                        onPressEnter={this.handleInputConfirm}
                    />
                )}
                {!inputVisible && (
                    <Tag
                        onClick={this.showInput}
                        style={{ background: '#fff', borderStyle: 'dashed' }}
                    >
                        <Icon type="plus" /> {placeholder}
                    </Tag>
                )}
            </div>
        );
    }
}

export class AntUpload extends React.Component<any, {}> {
    onChange = info => {
        const { field, form } = this.props;
        let fileList = info.fileList;

        // 1. Limit the number of uploaded files
        // Only to show two recent uploaded files, and old ones will be replaced by the new
        fileList = fileList.slice(-2);

        // 2. Read from response and show file link
        fileList = fileList.map(file => {
            if (file.response) {
                // Component will show file.url as link
                file.url = file.response.url;
            }
            return file;
        });

        // 3. Filter successfully uploaded files according to response from server
        fileList = fileList.filter(file => {
            if (file.response) {
                return file.response.status === 'success';
            }
            return false;
        });

        form.setFieldValue(field.name, fileList.map(file => file.url));
    };

    beforeUpload = file => {
        const { maxFileSize } = this.props;
        if (file.size > maxFileSize) {
            notification.error({ message: intl.get('ErrorFileSize') });
            return false;
        }
        return true;
    };

    render() {
        const { form, field, uploadUrl, headers, onRemove } = this.props;

        return (
            <Upload
                accept="application/pdf,"
                action={uploadUrl}
                beforeUpload={this.beforeUpload}
                onChange={this.onChange}
                headers={headers}
                name={field.name}
                onRemove={file => onRemove(file, form, field)}
            >
                <Button>
                    <Icon type="upload" />
                    {intl.get('UploadResume')}
                </Button>
            </Upload>
        );
    }
}

export const AntSelect = CreateAntField(Select);
export const AntDatePicker = CreateAntField(DatePicker);
export const AntInput = CreateAntField(Input);
export const AntTimePicker = CreateAntField(TimePicker);
export const AntTextArea = CreateAntField(TextArea);
export const AntRadioGroup = CreateAntRadioField(Radio.Group);
