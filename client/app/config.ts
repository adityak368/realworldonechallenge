export default {
    HOST: 'http://localhost:3000',
    API: '/api/v1',
    brokerHost: 'cenrifugo-rwc.herokuapp.com',
    brokerPort: 8000
};
