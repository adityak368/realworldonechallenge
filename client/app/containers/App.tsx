import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Store } from 'redux';
import { Router } from 'react-router-dom';
import { History } from 'history';
import { AppState } from 'data/reducers';
import { ThemeProvider } from 'react-jss';

import 'styles';
import AppTheme from 'styles/theme';
import Root from 'containers/Root';
import ErrorBoundary from 'components/ErrorBoundary';

interface Props {
    store: Store<AppState>;
    history: History;
}

export default class App extends Component<Props, {}> {
    render() {
        const { store, history } = this.props;
        return (
            <ErrorBoundary>
                <Provider store={store}>
                    <Router history={history}>
                        <ThemeProvider theme={AppTheme}>
                            <Root />
                        </ThemeProvider>
                    </Router>
                </Provider>
            </ErrorBoundary>
        );
    }
}
