import React, { Component } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import intl from 'common/intl';

import { FormikActions } from 'formik';
import { ChangePasswordForm } from 'common/form';
import ChangePassword from 'pages/ChangePassword';

import authActions from 'data/actions/auth';

import { AppState } from 'data/reducers';
import { IAuthState } from 'data/reducers/auth';

import notification from 'common/notification';

const mapDispatchToProps = (dispatch: Dispatch) => ({
    authActions: bindActionCreators(authActions, dispatch)
});

const mapStateToProps = (state: AppState) => ({
    auth: state.auth
});

interface StateProps {
    auth: IAuthState;
}

interface DispatchProps {
    authActions: typeof authActions;
}

interface OwnProps {}

type Props = StateProps & DispatchProps & OwnProps;

class ChangePasswordContainer extends Component<
    RouteComponentProps<Props>,
    {}
> {
    onChangePassword = (
        values: ChangePasswordForm,
        actions: FormikActions<ChangePasswordForm>
    ) => {
        const { history, location, authActions } = this.props;
        actions.setSubmitting(true);
        window.authManager
            .iChangePassword(values)
            .then(async res => {
                actions.setSubmitting(false);
                authActions.logout();
                notification.info({ message: intl.get('PasswordChanged') });
                history.push('/login');
            })
            .catch(error => {
                actions.setSubmitting(false);
                notification.error({ message: error.message });
            });
    };

    render() {
        const { auth, authActions } = this.props;
        return (
            <ChangePassword
                onChangePassword={this.onChangePassword}
                auth={auth}
                authActions={authActions}
            />
        );
    }
}

export default withRouter(
    connect<StateProps, DispatchProps, OwnProps>(
        mapStateToProps,
        mapDispatchToProps
    )(ChangePasswordContainer)
);
