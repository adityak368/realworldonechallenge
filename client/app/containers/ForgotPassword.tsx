import React, { Component } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import { FormikActions } from 'formik';
import { ForgotPasswordForm } from 'common/form';
import ForgotPassword from 'pages/ForgotPassword';

import authActions from 'data/actions/auth';

import { AppState } from 'data/reducers';
import { IAuthState } from 'data/reducers/auth';

import notification from 'common/notification';

const mapDispatchToProps = (dispatch: Dispatch) => ({
    authActions: bindActionCreators(authActions, dispatch)
});

const mapStateToProps = (state: AppState) => ({
    auth: state.auth
});

interface StateProps {
    auth: IAuthState;
}

interface DispatchProps {
    authActions: typeof authActions;
}

interface OwnProps {}

type Props = StateProps & DispatchProps & OwnProps;

class ForgotPasswordContainer extends Component<
    RouteComponentProps<Props>,
    {}
> {
    onSubmit = (
        values: ForgotPasswordForm,
        actions: FormikActions<ForgotPasswordForm>
    ) => {
        const { history } = this.props;
        actions.setSubmitting(true);
        window.authManager
            .iForgotPassword(values)
            .then(async (res: any) => {
                actions.setSubmitting(false);
                notification.info({ message: res.message });
                history.push('/login');
            })
            .catch(error => {
                actions.setSubmitting(false);
                notification.error({ message: error.message });
            });
    };

    render() {
        const { auth, authActions } = this.props;
        return (
            <ForgotPassword
                onSubmit={this.onSubmit}
                auth={auth}
                authActions={authActions}
            />
        );
    }
}

export default withRouter(
    connect<StateProps, DispatchProps, OwnProps>(
        mapStateToProps,
        mapDispatchToProps
    )(ForgotPasswordContainer)
);
