import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import intl from 'common/intl';
import Header from 'layouts/Header';
import authActions from 'data/actions/auth';
import notificationActions from 'data/actions/notification';
import { AppState } from 'data/reducers';
import { IAuthState } from 'data/reducers/auth';
import { INotificationState } from 'data/reducers/notification';

const mapDispatchToProps = (dispatch: Dispatch) => ({
    notificationActions: bindActionCreators(notificationActions, dispatch),
    authActions: bindActionCreators(authActions, dispatch)
});

const mapStateToProps = (state: AppState) => ({
    auth: state.auth,
    notifications: state.notifications
});

interface StateProps {
    auth: IAuthState;
    notifications: INotificationState;
}

interface DispatchProps {
    authActions: typeof authActions;
    notificationActions: typeof notificationActions;
}

interface OwnProps {
    changeSideBarState: (isSideBarOpen: boolean) => void;
    changeDrawerState: (drawerIsOpen: boolean) => void;
    isSideBarOpen: boolean;
    left: Number;
    drawerIsOpen: boolean;
    isMobile: boolean;
}

type Props = StateProps & DispatchProps & OwnProps;

class HeaderContainer extends Component<RouteComponentProps<Props>, {}> {
    onLogout = () => {
        const { authActions, history } = this.props;
        authActions.logout();
        history.push('/login');
    };

    onLogin = () => {
        const { history } = this.props;
        history.push('/login');
    };

    changeLocale = (locale: string) => {
        intl.setLocale(locale);
    };

    clearNotifications = () => {
        const { notificationActions } = this.props;
        notificationActions.clearAll();
    };

    render() {
        const {
            left,
            changeSideBarState,
            changeDrawerState,
            isSideBarOpen,
            drawerIsOpen,
            isMobile,
            auth,
            notifications
        } = this.props;

        return (
            <Header
                auth={auth}
                left={left}
                changeSideBarState={changeSideBarState}
                changeDrawerState={changeDrawerState}
                isSideBarOpen={isSideBarOpen}
                drawerIsOpen={drawerIsOpen}
                isMobile={isMobile}
                onLogin={this.onLogin}
                onLogout={this.onLogout}
                changeLocale={this.changeLocale}
                clearNotifications={this.clearNotifications}
                notifications={notifications}
            />
        );
    }
}

export default withRouter(
    connect<StateProps, DispatchProps, OwnProps>(
        mapStateToProps,
        mapDispatchToProps
    )(HeaderContainer)
);
