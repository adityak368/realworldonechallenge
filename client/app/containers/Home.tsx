import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import Home from 'pages/Home';

import { AppState } from 'data/reducers';
import { IAuthState } from 'data/reducers/auth';
import authActions from 'data/actions/auth';
import contactsActions from 'data/actions/contacts';

import notification from 'common/notification';
import { IContactState } from 'data/reducers/contacts';
import DeviceCapture from 'lib/capture/DeviceCapture';
import {
    BrokerSignals,
    Handshake,
    GenerateBrokerSignal,
    Namespaces
} from 'lib/comm/messaging/types';
import intl from 'common/intl';
import { MediaType } from 'common/types';
import Logger from 'common/logger';

import Peer from 'simple-peer';
import update from 'immutability-helper';
import { When, If, Then, Else } from 'react-if';
import Loader from 'components/Loader';

const mapDispatchToProps = (dispatch: Dispatch) => ({
    authActions: bindActionCreators(authActions, dispatch),
    contactsActions: bindActionCreators(contactsActions, dispatch)
});

const mapStateToProps = (state: AppState) => ({
    auth: state.auth,
    contacts: state.contacts
});

interface StateProps {
    auth: IAuthState;
    contacts: IContactState;
}

interface DispatchProps {
    authActions: typeof authActions;
    contactsActions: typeof contactsActions;
}

interface OwnProps {}

type Props = StateProps & DispatchProps & OwnProps;

class HomeContainer extends Component<RouteComponentProps<Props>, {}> {
    deviceCapture: DeviceCapture = new DeviceCapture();

    componentDidMount() {
        const { contactsActions } = this.props;
        this.fetchOnlineUsers();
        try {
            // exchanging offers and answers
            window.broker.on(BrokerSignals.Handshake, (message: Handshake) => {
                const { contacts } = this.props;
                const { handshakeRequester, connData } = message;

                let peer = contacts.connections[handshakeRequester];
                if (!peer) {
                    peer = this.initiateConnection(handshakeRequester, false);
                }
                if (peer) {
                    peer.signal(connData);
                }
            });

            // exchanging offers and answers
            window.broker.on(BrokerSignals.PeerJoined, (user: IContact) => {
                const { contacts, auth, contactsActions } = this.props;
                //because we received current machine joined message too
                if (user.username === auth.user.username) {
                    return;
                }
                contactsActions.contactConnected(user);
                let peer = contacts.connections[user.username];
                // if (!peer) {
                //     peer = this.initiateConnection(user.username, true);
                // }
                notification.info({
                    message: intl.getf('UserJoined', user.username)
                });
            });

            // destroy all existing connections
            window.broker.on(BrokerSignals.PeerLeft, (user: IContact) => {
                const { contacts, contactsActions, auth } = this.props;
                // because we received current machine joined message too
                if (user.username === auth.user.username) {
                    return;
                }
                contactsActions.contactDisconnected(user);
                const peer = contacts.connections[user.username];
                if (peer) {
                    peer.destroy();
                }
                notification.info({
                    message: intl.getf('UserLeft', user.username)
                });
            });

            window.broker.on(BrokerSignals.ChatMessage, message => {
                const { sender, msg } = message;
                contactsActions.receivedMsgFromUser({
                    username: sender,
                    msg
                });
            });
            this.setDefaultMediaDevices();
        } catch (err) {
            Logger.error(err);
            notification.error({ message: err.message });
        }
    }

    componentWillUnmount() {
        window.broker.removeAllListeners(BrokerSignals.ChatMessage);
        window.broker.removeAllListeners(BrokerSignals.Handshake);
        window.broker.removeAllListeners(BrokerSignals.PeerJoined);
        window.broker.removeAllListeners(BrokerSignals.PeerLeft);
    }

    startStream = async (mediaType: MediaType) => {
        return new Promise(async (resolve, reject) => {
            try {
                const { contacts, contactsActions } = this.props;
                const device = contacts.media[mediaType];
                if (device) {
                    const stream = await this.deviceCapture.getStream({
                        [mediaType]: { deviceId: device.deviceId }
                    });
                    contactsActions.updateCurrentMediaDevice({
                        mediaType: mediaType,
                        device: update(device, {
                            stream: { $set: stream },
                            isEnabled: { $set: true }
                        })
                    });
                    Object.keys(contacts.connections).forEach(key => {
                        contacts.connections[key].addStream(stream);
                    });
                    resolve();
                }
            } catch (err) {
                Logger.error(err);
                switch (err.name) {
                    case 'NotAllowedError':
                        notification.error({
                            message: intl.get('MediaPermissionError')
                        });
                        break;
                    case 'NotReadableError':
                        notification.error({
                            message: intl.get('ErrorCapturingInputFromDevice')
                        });
                        break;
                    default:
                        notification.error({
                            message: intl.get('UnkownError')
                        });
                        break;
                }
                reject();
            }
        });
    };

    onVideoShare = async (username: string) => {
        const { contacts }: Props = this.props;
        if (!username) {
            notification.error({ message: intl.get('NoUserSelected') });
            return;
        }
        const currentMediaDevice = contacts.media[MediaType.Video];
        if (!currentMediaDevice) {
            notification.error({
                message: intl.get('ErrorCapturingInputFromDevice')
            });
            return;
        }
        if (!currentMediaDevice.stream) {
            // No Stream available. So start streaming
            await this.startStream(MediaType.Video);
        } else {
            this.onMediaButtonClick(MediaType.Video);
        }
        if (!contacts.connections[username]) {
            this.initiateConnection(username, true);
        }
    };

    initiateConnection = (username: string, isSessionInitiator: boolean) => {
        const { contactsActions, auth, contacts } = this.props;
        const { media } = contacts;
        if (username === auth.user.username) {
            return null;
        }

        const peerOptions: any = {
            initiator: isSessionInitiator,
            trickle: false,
            objectMode: true
        };

        const streams = [];
        if (media.audio && media.audio.stream) {
            streams.push(media.audio.stream);
        }
        if (media.video && media.video.stream) {
            streams.push(media.video.stream);
        }
        if (streams.length) {
            peerOptions.streams = streams;
        }
        const peer = new Peer(peerOptions);
        peer.on('signal', connData => {
            const { auth }: Props = this.props;
            window.contactManager
                .iRelay(
                    username,
                    GenerateBrokerSignal(BrokerSignals.Handshake, {
                        handshakeRequester: auth.user.username,
                        connData
                    })
                )
                .catch(err => {
                    Logger.error(err);
                    notification.error({ message: err.message });
                });
        });
        peer.on('connect', () => {
            const { contacts } = this.props;
            const { media } = contacts;
            if (media.audio && media.audio.stream) {
                window.contactManager.iBroadCastAudioVideoState(
                    contacts.connections,
                    auth.user.username,
                    media.audio.stream.id,
                    media.audio.stream.getTracks().some(t => t.enabled === true)
                );
            }
            if (media.video && media.video.stream) {
                window.contactManager.iBroadCastAudioVideoState(
                    contacts.connections,
                    auth.user.username,
                    media.video.stream.id,
                    media.video.stream.getTracks().some(t => t.enabled === true)
                );
            }
        });
        peer.on('data', data => {
            const { data: parsedData, type } = JSON.parse(data);
            switch (type) {
                case BrokerSignals.UpdateAudioVideoState:
                    contactsActions.updateContactStream({
                        username: parsedData.username,
                        streamID: parsedData.streamID,
                        isEnabled: parsedData.isEnabled
                    });
                    break;
                default:
                    break;
            }
        });
        peer.on('stream', stream => {
            contactsActions.addContactStream({
                username,
                stream
            });
        });
        peer.on('close', () => {
            contactsActions.contactDisconnected(username);
        });
        peer.on('error', err => {
            Logger.error(err);
            notification.error({ message: err.message });
            contactsActions.contactDisconnected(username);
        });
        contactsActions.onNewContactConnection({
            username,
            connection: peer
        });
        return peer;
    };

    fetchOnlineUsers = () => {
        const { contactsActions } = this.props;
        // window.contactManager
        //     .iFetchContacts()
        //     .then((result: Array<IContact>) => {
        //         contactsActions.addContacts(result);
        //     })
        //     .catch(err => notification.error({ message: err.message }));
        window.broker
            .getUserPresence()
            .then(({ presence }) => {
                const userset = new Set<IContact>();
                Object.keys(presence).forEach(key => {
                    userset.add({
                        ...presence[key].conn_info,
                        isOnline: true,
                        msg: []
                    });
                });
                contactsActions.addContacts(Array.from(userset));
            })
            .catch(err => notification.error({ message: err.message }));
    };

    onSendChatMsg = (username: string, msg: string) => {
        const { auth } = this.props;
        if (!username) {
            notification.error({ message: intl.get('NoUserSelected') });
            return;
        }
        if (!msg) {
            return;
        }
        const chatMsg = {
            text: msg,
            timestamp: new Date(),
            from: auth.user.username,
            to: username
        };
        const { contactsActions } = this.props;
        contactsActions.sendMsgToUser({
            username,
            msg: chatMsg
        });
        window.contactManager
            .iRelay(
                username,
                GenerateBrokerSignal(BrokerSignals.ChatMessage, {
                    msg: chatMsg,
                    sender: auth.user.username
                })
            )
            .catch(err => notification.error({ message: err.message }));
    };

    setDefaultMediaDevices = async () => {
        const { contactsActions } = this.props;
        const defaultAudioInput = await this.deviceCapture.getDefaultDeviceForMediaType(
            'audioinput'
        );
        const defaultVideoInput = await this.deviceCapture.getDefaultDeviceForMediaType(
            'videoinput'
        );
        const defaultAudioOutput = await this.deviceCapture.getDefaultDeviceForMediaType(
            'audiooutput'
        );
        contactsActions.updateCurrentMediaDevice({
            mediaType: 'audio',
            device: defaultAudioInput
        });
        contactsActions.updateCurrentMediaDevice({
            mediaType: 'video',
            device: defaultVideoInput
        });
        contactsActions.updateCurrentMediaDevice({
            mediaType: 'speaker',
            device: defaultAudioOutput
        });
    };

    getPeerStreamOfType = (
        username: string,
        mediaType: MediaType
    ): MediaStream => {
        if (username) {
            const { contacts } = this.props;
            const allStreams = contacts.streams[username] || null;
            if (allStreams) {
                const newMediaStream = this.deviceCapture.createNewMediaStream();
                allStreams.forEach(s => {
                    if (s.isEnabled) {
                        if (mediaType === MediaType.Audio) {
                            s.stream.getAudioTracks().forEach(track => {
                                newMediaStream.addTrack(track);
                            });
                        }
                        if (mediaType === MediaType.Video) {
                            s.stream.getVideoTracks().forEach(track => {
                                newMediaStream.addTrack(track);
                            });
                        }
                    }
                });
                return newMediaStream;
            }
        }
    };

    onMediaButtonClick = async (mediaType: MediaType) => {
        const { contacts, auth, contactsActions } = this.props;
        const currentMediaDevice = contacts.media[mediaType];
        if (!currentMediaDevice) {
            notification.error({
                message: intl.get('ErrorCapturingInputFromDevice')
            });
            return;
        }
        const device = Object.assign({}, currentMediaDevice);
        // already started streaming. Just disable the tracks.
        if (currentMediaDevice.stream) {
            const tracks = device.stream.getTracks();
            tracks.forEach(track => (track.enabled = !device.isEnabled));
            device.isEnabled = !device.isEnabled;
            window.contactManager.iBroadCastAudioVideoState(
                contacts.connections,
                auth.user.username,
                device.stream.id,
                device.isEnabled
            );
            contactsActions.updateCurrentMediaDevice({
                mediaType,
                device
            });
        }
    };

    render() {
        const { auth, contacts }: Props = this.props;

        return (
            <If condition={auth.isLoggedIn}>
                <Then>
                    <Home
                        auth={auth}
                        onSendChatMsg={this.onSendChatMsg}
                        contacts={contacts}
                        onVideoShare={this.onVideoShare}
                        getPeerStreamOfType={this.getPeerStreamOfType}
                    />
                </Then>
                <Else>
                    <Loader spinning />
                </Else>
            </If>
        );
    }
}

export default withRouter(
    connect<StateProps, DispatchProps, OwnProps>(
        mapStateToProps,
        mapDispatchToProps
    )(HomeContainer)
);
