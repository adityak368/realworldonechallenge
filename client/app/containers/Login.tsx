import React, { Component } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import { FormikActions } from 'formik';
import { LoginForm } from 'common/form';
import Login from 'pages/Login';

import authActions from 'data/actions/auth';

import { AppState } from 'data/reducers';
import { IAuthState } from 'data/reducers/auth';

import notification from 'common/notification';

const mapDispatchToProps = (dispatch: Dispatch) => ({
    authActions: bindActionCreators(authActions, dispatch)
});

const mapStateToProps = (state: AppState) => ({
    auth: state.auth
});

interface StateProps {
    auth: IAuthState;
}

interface DispatchProps {
    authActions: typeof authActions;
}

interface OwnProps {}

type Props = StateProps & DispatchProps & OwnProps;

class LoginContainer extends Component<RouteComponentProps<Props>, {}> {
    componentDidMount() {
        const { auth, location, history } = this.props;
        const { state } = location;
        if (!state) {
            if (auth.isLoggedIn) {
                history.push('/');
            }
            return;
        }
        const { from } = state;
        if (auth.isLoggedIn) {
            history.push(from || '/');
        }
    }

    onSubmit = (values: LoginForm, actions: FormikActions<LoginForm>) => {
        const { authActions, location, history } = this.props;

        actions.setSubmitting(true);
        window.authManager
            .iLogin(values)
            .then(async res => {
                actions.setSubmitting(false);
                authActions.login(res);
                const { state } = location;
                if (!state) {
                    history.push('/');
                    return;
                }
                const { from } = state;
                history.push(from || '/');
            })
            .catch(error => {
                actions.setSubmitting(false);
                notification.error({ message: error.message });
            });
    };

    render() {
        const { auth, authActions } = this.props;
        return (
            <Login
                onSubmit={this.onSubmit}
                auth={auth}
                authActions={authActions}
            />
        );
    }
}

export default withRouter(
    connect<StateProps, DispatchProps, OwnProps>(
        mapStateToProps,
        mapDispatchToProps
    )(LoginContainer)
);
