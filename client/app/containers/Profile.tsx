import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { bindActionCreators, Dispatch } from 'redux';

import Profile from 'pages/Profile';

import authActions from 'data/actions/auth';

import { AppState } from 'data/reducers';
import { IAuthState } from 'data/reducers/auth';
import { FormikActions } from 'formik';
import { ProfileForm, ChangePasswordForm } from 'common/form';
import notification from 'common/notification';

const mapDispatchToProps = (dispatch: Dispatch) => ({
    authActions: bindActionCreators(authActions, dispatch)
});

const mapStateToProps = (state: AppState) => ({
    auth: state.auth
});

interface StateProps {
    auth: IAuthState;
}

interface DispatchProps {
    authActions: typeof authActions;
}

interface OwnProps {}

type Props = StateProps & DispatchProps & OwnProps;

class ProfileContainer extends Component<RouteComponentProps<Props>, {}> {
    onSubmit = (values: ProfileForm, actions: FormikActions<ProfileForm>) => {
        const { authActions } = this.props;
        actions.setSubmitting(true);
        window.authManager
            .iUpdateProfile(values)
            .then(async res => {
                authActions.editProfile(res);
                actions.setSubmitting(false);
            })
            .catch(error => {
                actions.setSubmitting(false);
                notification.error({ message: error.message });
            });
    };

    render() {
        const { auth } = this.props;
        return <Profile onSubmit={this.onSubmit} auth={auth} />;
    }
}

export default withRouter(
    connect<StateProps, DispatchProps, OwnProps>(
        mapStateToProps,
        mapDispatchToProps
    )(ProfileContainer)
);
