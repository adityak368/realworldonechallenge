import React, { Component } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import intl from 'common/intl';

import Registration from 'pages/Registration';
import { FormikActions } from 'formik';
import { RegistrationForm } from 'common/form';

import authActions from 'data/actions/auth';

import { AppState } from 'data/reducers';
import { IAuthState } from 'data/reducers/auth';

import notification from 'common/notification';

const mapDispatchToProps = (dispatch: Dispatch) => ({
    authActions: bindActionCreators(authActions, dispatch)
});

const mapStateToProps = (state: AppState) => ({
    auth: state.auth
});

interface StateProps {
    auth: IAuthState;
}

interface DispatchProps {
    authActions: typeof authActions;
}

interface OwnProps {}

type Props = StateProps & DispatchProps & OwnProps;

class RegistrationContainer extends Component<RouteComponentProps<Props>, {}> {
    componentDidMount() {
        const { auth, location, history } = this.props;
        const { state } = location;
        if (!state) {
            if (auth.isLoggedIn) {
                history.push('/');
            }
            return;
        }
        const { from } = state;
        if (auth.isLoggedIn) {
            history.push(from || '/');
        }
    }

    onSubmit = (
        values: RegistrationForm,
        actions: FormikActions<RegistrationForm>
    ) => {
        const { history } = this.props;

        actions.setSubmitting(true);
        window.authManager
            .iSignUp(values)
            .then(res => {
                actions.setSubmitting(false);
                notification.success({
                    message: intl.getf('CreatedNewUser', values.email)
                });
                history.push('/login');
            })
            .catch(error => {
                actions.setSubmitting(false);
                notification.error({ message: error.message });
            });
    };

    render() {
        const { auth, authActions } = this.props;
        return (
            <Registration
                onSubmit={this.onSubmit}
                auth={auth}
                authActions={authActions}
            />
        );
    }
}

export default withRouter(
    connect<StateProps, DispatchProps, OwnProps>(
        mapStateToProps,
        mapDispatchToProps
    )(RegistrationContainer)
);
