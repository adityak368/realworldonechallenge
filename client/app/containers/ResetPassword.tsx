import React, { Component } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import intl from 'common/intl';

import { FormikActions } from 'formik';
import { ResetPasswordForm } from 'common/form';
import ResetPassword from 'pages/ResetPassword';

import authActions from 'data/actions/auth';

import { AppState } from 'data/reducers';
import { IAuthState } from 'data/reducers/auth';

import notification from 'common/notification';

import queryString from 'query-string';

const mapDispatchToProps = (dispatch: Dispatch) => ({
    authActions: bindActionCreators(authActions, dispatch)
});

const mapStateToProps = (state: AppState) => ({
    auth: state.auth
});

interface StateProps {
    auth: IAuthState;
}

interface DispatchProps {
    authActions: typeof authActions;
}

interface OwnProps {}

type Props = StateProps & DispatchProps & OwnProps;

class ResetPasswordContainer extends Component<RouteComponentProps<Props>, {}> {
    onSubmit = (
        values: ResetPasswordForm,
        actions: FormikActions<ResetPasswordForm>
    ) => {
        const { history, location } = this.props;
        const parsed = queryString.parse(location.search);
        actions.setSubmitting(true);
        values.token = parsed.t as string;
        window.authManager
            .iResetPassword(values)
            .then(async res => {
                actions.setSubmitting(false);
                notification.info({ message: intl.get('PasswordChanged') });
                history.push('/login');
            })
            .catch(error => {
                actions.setSubmitting(false);
                notification.error({ message: error.message });
            });
    };

    render() {
        const { auth, authActions } = this.props;
        return (
            <ResetPassword
                onSubmit={this.onSubmit}
                auth={auth}
                authActions={authActions}
            />
        );
    }
}

export default withRouter(
    connect<StateProps, DispatchProps, OwnProps>(
        mapStateToProps,
        mapDispatchToProps
    )(ResetPasswordContainer)
);
