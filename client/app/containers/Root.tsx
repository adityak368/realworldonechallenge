import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { RouteComponentProps, withRouter, Switch } from 'react-router-dom';

import authActions from 'data/actions/auth';
import notificationActions from 'data/actions/notification';

import { AppState } from 'data/reducers';
import { IAuthState } from 'data/reducers/auth';
import { INotificationState } from 'data/reducers/notification';

import { PrivateRoute, PropsRoute } from 'routing/routes';
import ROUTES_ITEMS from 'routing/routedefs';
import { BrokerSignals } from 'lib/comm/messaging/types';

const mapDispatchToProps = (dispatch: Dispatch) => ({
    authActions: bindActionCreators(authActions, dispatch),
    notificationActions: bindActionCreators(notificationActions, dispatch)
});

const mapStateToProps = (state: AppState) => ({
    auth: state.auth,
    notifications: state.notifications
});

interface StateProps {
    auth: IAuthState;
    notifications: INotificationState;
}

interface DispatchProps {
    authActions: typeof authActions;
    notificationActions: typeof notificationActions;
}

interface OwnProps {}

type Props = StateProps & DispatchProps & OwnProps;

interface State {
    user: IUser | null;
}

class RootContainer extends Component<RouteComponentProps<Props>, State> {
    state = {
        user: null
    };

    componentWillReceiveProps(nextProps) {
        const { user } = this.state;

        if (user === null && nextProps.auth.user !== null) {
            window.broker.connect({
                authToken: nextProps.auth.user.authToken,
                username: nextProps.auth.user.username
            });
            window.broker.listenForCommands();
            window.broker.listenForContactStatus();
        }
        this.setState({
            user: nextProps.auth.user
        });
    }

    componentDidMount() {
        const { authActions } = this.props;
        this.loginIfPossible();
        this.fetchAppData();

        window.broker.on(BrokerSignals.ConnectedToServer, () => {
            authActions.serverConnectionStateChange(true);
        });

        window.broker.on(BrokerSignals.Disconnect, () => {
            authActions.serverConnectionStateChange(false);
        });
    }

    componentWillUnmount() {
        window.broker.removeAllListeners(BrokerSignals.ConnectedToServer);
        window.broker.removeAllListeners(BrokerSignals.Disconnect);
    }

    loginIfPossible = () => {
        const { authActions } = this.props;
        const token: string = window.localStorage.getItem('authToken');
        if (token) {
            authActions.login({
                authToken: token
            });
        }
    };

    fetchAppData = () => {
        const { notificationActions } = this.props;
        notificationActions.addNotifications([
            {
                title: 'New User is registered.',
                date: new Date(Date.now() - 10000000),
                text: ''
            },
            {
                title: 'Application has been approved.',
                date: new Date(Date.now() - 50000000),
                text: ''
            }
        ]);
    };

    render() {
        const { auth, authActions } = this.props;

        return (
            <Switch>
                {ROUTES_ITEMS.map(route => {
                    if (!route.isProtected) {
                        return (
                            <PropsRoute
                                key={route.path}
                                auth={auth}
                                authActions={authActions}
                                {...route}
                            />
                        );
                    }
                    return (
                        <PrivateRoute
                            key={route.path}
                            auth={auth}
                            authActions={authActions}
                            {...route}
                        />
                    );
                })}
            </Switch>
        );
    }
}

export default withRouter(
    connect<StateProps, DispatchProps, OwnProps>(
        mapStateToProps,
        mapDispatchToProps
    )(RootContainer)
);
