import {
    Action,
    PayloadedAction,
    createAction,
    createPayloadedAction
} from 'data/actions/action-helper';

export enum AuthActionConstants {
    LOGIN = 'LOGIN',
    LOGOUT = 'LOGOUT',
    SERVER_CONNECTION_STATE_CHANGE = 'SERVER_CONNECTION_STATE_CHANGE',
    EDIT_PROFILE = 'EDIT_PROFILE'
}

// Action<type> type => used in reducer switch
export interface ILogin
    extends PayloadedAction<AuthActionConstants.LOGIN, { authToken: string }> {}

export interface ILogout extends Action<AuthActionConstants.LOGOUT> {}

export interface IEditProfile
    extends PayloadedAction<
        AuthActionConstants.EDIT_PROFILE,
        { authToken: string }
    > {}

export interface IServerConnectionStateChange
    extends PayloadedAction<
        AuthActionConstants.SERVER_CONNECTION_STATE_CHANGE,
        boolean
    > {}

export type AuthActions =
    | ILogin
    | ILogout
    | IServerConnectionStateChange
    | IEditProfile;

const login = createPayloadedAction<ILogin>(AuthActionConstants.LOGIN);
const logout = createAction<ILogout>(AuthActionConstants.LOGOUT);

const editProfile = createPayloadedAction<IEditProfile>(
    AuthActionConstants.EDIT_PROFILE
);

const serverConnectionStateChange = createPayloadedAction<
    IServerConnectionStateChange
>(AuthActionConstants.SERVER_CONNECTION_STATE_CHANGE);

export default {
    login,
    logout,
    editProfile,
    serverConnectionStateChange
};
