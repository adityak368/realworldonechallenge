import {
    Action,
    PayloadedAction,
    createAction,
    createPayloadedAction
} from 'data/actions/action-helper';

import Peer from 'simple-peer';
import { MediaType } from 'common/types';

export enum ContactActionConstants {
    ADD_CONTACTS = 'ADD_CONTACTS',
    RECEIVED_MSG_FROM_USER = 'RECEIVED_MSG_FROM_USER',
    SEND_MSG_TO_USER = 'SEND_MSG_TO_USER',
    CONTACT_CONNECTED = 'CONTACT_CONNECTED',
    ON_NEW_CONTACT_CONNECTION = 'ON_NEW_CONTACT_CONNECTION',
    CONTACT_DISCONNECTED = 'CONTACT_DISCONNECTED',
    ADD_CONTACT_STREAM = 'ADD_CONTACT_STREAM',
    UPDATE_CONTACT_STREAM_STATE = 'UPDATE_CONTACT_STREAM_STATE',
    UPDATE_CURRENT_MEDIA_DEVICE = 'UPDATE_CURRENT_MEDIA_DEVICE',
    CLOSE_MEDIA_DEVICE = 'CLOSE_MEDIA_DEVICE'
}

// Action<type> type => used in reducer switch
export interface IAddContacts
    extends PayloadedAction<
        ContactActionConstants.ADD_CONTACTS,
        Array<IContact>
    > {}

export interface IReceivedMsgFromUser
    extends PayloadedAction<
        ContactActionConstants.RECEIVED_MSG_FROM_USER,
        { username: string; msg: ChatMsg }
    > {}

export interface ISendMsgToUser
    extends PayloadedAction<
        ContactActionConstants.SEND_MSG_TO_USER,
        { username: string; msg: ChatMsg }
    > {}

export interface IContactConnected
    extends PayloadedAction<
        ContactActionConstants.CONTACT_CONNECTED,
        IContact
    > {}

export interface IContactDisconnected
    extends PayloadedAction<
        ContactActionConstants.CONTACT_DISCONNECTED,
        IContact
    > {}

export interface IOnNewContactConnection
    extends PayloadedAction<
        ContactActionConstants.ON_NEW_CONTACT_CONNECTION,
        { username: string; connection: Peer }
    > {}

export interface IAddContactStream
    extends PayloadedAction<
        ContactActionConstants.ADD_CONTACT_STREAM,
        { username: string; stream: MediaStream }
    > {}

export interface IUpdateContactStreamState
    extends PayloadedAction<
        ContactActionConstants.UPDATE_CONTACT_STREAM_STATE,
        { username: number; streamID: string; isEnabled: boolean }
    > {}

export interface IUpdateCurrentMediaDevice
    extends PayloadedAction<
        ContactActionConstants.UPDATE_CURRENT_MEDIA_DEVICE,
        { mediaType: MediaType; device: IMediaDevice }
    > {}

export interface ICloseMediaDevice
    extends PayloadedAction<
        ContactActionConstants.CLOSE_MEDIA_DEVICE,
        MediaType
    > {}

export type ContactActions =
    | IAddContacts
    | IReceivedMsgFromUser
    | ISendMsgToUser
    | IContactConnected
    | IContactDisconnected
    | IOnNewContactConnection
    | IAddContactStream
    | IUpdateContactStreamState
    | IUpdateCurrentMediaDevice
    | ICloseMediaDevice;

const addContacts = createPayloadedAction<IAddContacts>(
    ContactActionConstants.ADD_CONTACTS
);
const receivedMsgFromUser = createPayloadedAction<IReceivedMsgFromUser>(
    ContactActionConstants.RECEIVED_MSG_FROM_USER
);
const sendMsgToUser = createPayloadedAction<ISendMsgToUser>(
    ContactActionConstants.SEND_MSG_TO_USER
);
const contactConnected = createPayloadedAction<IContactConnected>(
    ContactActionConstants.CONTACT_CONNECTED
);
const contactDisconnected = createPayloadedAction<IContactDisconnected>(
    ContactActionConstants.CONTACT_DISCONNECTED
);
const onNewContactConnection = createPayloadedAction<IOnNewContactConnection>(
    ContactActionConstants.ON_NEW_CONTACT_CONNECTION
);
const addContactStream = createPayloadedAction<IAddContactStream>(
    ContactActionConstants.ADD_CONTACT_STREAM
);
const updateContactStream = createPayloadedAction<IUpdateContactStreamState>(
    ContactActionConstants.UPDATE_CONTACT_STREAM_STATE
);

const updateCurrentMediaDevice = createPayloadedAction<
    IUpdateCurrentMediaDevice
>(ContactActionConstants.UPDATE_CURRENT_MEDIA_DEVICE);

const closeMediaDevice = createPayloadedAction<ICloseMediaDevice>(
    ContactActionConstants.CLOSE_MEDIA_DEVICE
);

export default {
    addContacts,
    receivedMsgFromUser,
    sendMsgToUser,
    contactConnected,
    contactDisconnected,
    onNewContactConnection,
    addContactStream,
    updateContactStream,
    updateCurrentMediaDevice,
    closeMediaDevice
};
