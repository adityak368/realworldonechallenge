import {
    Action,
    PayloadedAction,
    createAction,
    createPayloadedAction
} from 'data/actions/action-helper';

export enum NotificationActionConstants {
    ADD = 'ADD',
    CLEAR_ALL = 'CLEAR_ALL'
}

// Action<type> type => used in reducer switch
export interface IAddNotification
    extends PayloadedAction<
        NotificationActionConstants.ADD,
        Array<INotification>
    > {}

export interface IClearAllNotification
    extends PayloadedAction<NotificationActionConstants.CLEAR_ALL, any> {}

export type NotificationActions = IAddNotification | IClearAllNotification;

const addNotifications = createPayloadedAction<IAddNotification>(
    NotificationActionConstants.ADD
);
const clearAll = createPayloadedAction<IClearAllNotification>(
    NotificationActionConstants.CLEAR_ALL
);

export default {
    addNotifications,
    clearAll
};
