import update from 'immutability-helper';
import { decodeToken } from 'common/utils';
import { AuthActions, AuthActionConstants } from 'data/actions/auth';

export interface IAuthState {
    isLoggedIn: boolean;
    isConnectedToServer: boolean;
    user: IUser | null;
}

const authState: IAuthState = {
    isLoggedIn: false,
    isConnectedToServer: false,
    user: null
};

export const initialAuthState = authState;

const authReducer = (
    state: IAuthState = initialAuthState,
    action: AuthActions
): IAuthState => {
    switch (action.type) {
        case AuthActionConstants.LOGIN:
        case AuthActionConstants.EDIT_PROFILE: {
            const { authToken } = action.payload;
            const newState = { ...state };
            const user: IUser = decodeToken(authToken);
            if (user) {
                newState.isLoggedIn = true;
                newState.user = user;
                window.localStorage.setItem('authToken', authToken);
            }
            return newState;
        }
        case AuthActionConstants.SERVER_CONNECTION_STATE_CHANGE: {
            return update(state, {
                isConnectedToServer: { $set: action.payload }
            });
        }
        default:
            return state;
    }
};

export default authReducer;
