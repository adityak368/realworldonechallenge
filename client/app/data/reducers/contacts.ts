import update from 'immutability-helper';
import Peer from 'simple-peer';
import { ContactActions, ContactActionConstants } from 'data/actions/contacts';

export interface IContactState {
    contacts: Array<IContact>;
    connections: {
        [username: string]: Peer;
    };
    streams: {
        [username: string]: Peer;
    };
    media: {
        audio: IMediaDevice | null;
        video: IMediaDevice | null;
        speaker: IMediaDevice | null;
    };
}

const contactState: IContactState = {
    contacts: [],
    connections: {},
    media: {
        audio: null,
        video: null,
        speaker: null
    },
    streams: {}
};

export const initialContactState = contactState;

const contactReducer = (
    state: IContactState = initialContactState,
    action: ContactActions
): IContactState => {
    switch (action.type) {
        case ContactActionConstants.ADD_CONTACTS: {
            const contacts = action.payload;
            return update(state, {
                contacts: { $set: contacts }
            });
        }
        case ContactActionConstants.RECEIVED_MSG_FROM_USER:
        case ContactActionConstants.SEND_MSG_TO_USER: {
            const { username, msg: message } = action.payload;
            const userIndex = state.contacts.findIndex(
                c => c.username === username
            );
            if (userIndex !== -1) {
                return update(state, {
                    contacts: {
                        [userIndex]: contact =>
                            update(contact, {
                                msg: msg =>
                                    update(msg || [], { $push: [message] })
                            })
                    }
                });
            }
            return state;
        }
        case ContactActionConstants.ON_NEW_CONTACT_CONNECTION: {
            const { username, connection } = action.payload;
            return update(state, {
                connections: {
                    [username]: { $set: connection }
                }
            });
        }
        case ContactActionConstants.ADD_CONTACT_STREAM: {
            const { username, stream } = action.payload;
            return update(state, {
                streams: {
                    $apply: streams => {
                        const streamsClone = Object.assign({}, streams);
                        if (streamsClone[username]) {
                            streamsClone[username].push({
                                stream,
                                isEnabled: true
                            });
                        } else {
                            streamsClone[username] = [
                                {
                                    stream,
                                    isEnabled: true
                                }
                            ];
                        }
                        return streamsClone;
                    }
                }
            });
        }
        case ContactActionConstants.UPDATE_CONTACT_STREAM_STATE: {
            const { username, streamID, isEnabled } = action.payload;
            const streamIndex = state.streams[username].findIndex(
                s => s.stream.id === streamID
            );
            if (streamIndex >= 0) {
                return update(state, {
                    streams: {
                        [username]: {
                            [streamIndex]: {
                                $apply: stream => {
                                    const streamClone = Object.assign(
                                        {},
                                        stream
                                    );
                                    streamClone.isEnabled = isEnabled;
                                    return streamClone;
                                }
                            }
                        }
                    }
                });
            }
            return state;
        }
        case ContactActionConstants.CONTACT_CONNECTED: {
            const contact = action.payload;
            const userIndex = state.contacts.findIndex(
                c => c.username === contact.username
            );
            if (userIndex === -1) {
                return update(state, {
                    contacts: { $push: [contact] }
                });
            }
            return state;
        }
        case ContactActionConstants.CONTACT_DISCONNECTED: {
            const contact = action.payload;
            const userIndex = state.contacts.findIndex(
                c => c.username === contact.username
            );
            if (userIndex !== -1) {
                return update(state, {
                    connections: { $unset: [contact.username] },
                    streams: { $unset: [contact.username] },
                    contacts: { $splice: [[userIndex, 1]] }
                });
            }
            return state;
        }
        case ContactActionConstants.UPDATE_CURRENT_MEDIA_DEVICE: {
            const { mediaType, device } = action.payload;
            return update(state, {
                media: {
                    [mediaType]: {
                        $set: device
                    }
                }
            });
        }
        case ContactActionConstants.CLOSE_MEDIA_DEVICE: {
            const mediaType = action.payload;
            return update(state, {
                media: {
                    [mediaType]: {
                        $apply: device => {
                            if (device && device.stream) {
                                device.stream
                                    .getTracks()
                                    .forEach(t => t.stop());
                            }
                            return null;
                        }
                    }
                }
            });
        }
        default:
            return state;
    }
};

export default contactReducer;
