import { combineReducers } from 'redux';

// reducer imports
import authReducer, { IAuthState, initialAuthState } from 'data/reducers/auth';
import notificationReducer, {
    INotificationState,
    initialNotificationState
} from 'data/reducers/notification';
import { AuthActionConstants } from 'data/actions/auth';
import contactsReducer, {
    initialContactState,
    IContactState
} from 'data/reducers/contacts';

export interface AppState {
    auth: IAuthState;
    contacts: IContactState;
    notifications: INotificationState;
}

export const initialAppState: AppState = {
    auth: initialAuthState,
    contacts: initialContactState,
    notifications: initialNotificationState
};

export default function createRootReducer(history) {
    const appReducer = combineReducers<AppState>({
        auth: authReducer,
        contacts: contactsReducer,
        notifications: notificationReducer
    });

    return (state, action) => {
        if (action.type === AuthActionConstants.LOGOUT) {
            window.localStorage.removeItem('authToken');
            state = undefined;
        }
        return appReducer(state, action);
    };
}
