import update from 'immutability-helper';
import {
    NotificationActions,
    NotificationActionConstants
} from 'data/actions/notification';

export interface INotificationState extends Array<INotification> {}

export const initialNotificationState: INotificationState = [];

const notificationReducer = (
    state = initialNotificationState,
    action: NotificationActions
): INotificationState => {
    switch (action.type) {
        case NotificationActionConstants.ADD: {
            const { payload } = action;
            return update(state, { $push: payload });
        }
        case NotificationActionConstants.CLEAR_ALL: {
            return [];
        }
        default:
            return state;
    }
};

export default notificationReducer;
