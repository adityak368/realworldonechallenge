import { createStore, applyMiddleware, Store } from 'redux';
import thunk from 'redux-thunk';
import { createBrowserHistory, History } from 'history';
import { createLogger } from 'redux-logger';
import createRootReducer, { AppState, initialAppState } from 'data/reducers';

const history: History = createBrowserHistory();

const rootReducer = createRootReducer(history);

const configureStore = (initialState: AppState = initialAppState): Store<AppState> => {
    // Redux Configuration
    const middleware = [];

    // Thunk Middleware
    middleware.push(thunk);

    // Logging Middleware
    const logger = createLogger({
        level: 'info',
        collapsed: true
    });

    // Skip redux logs in console during the tests
    if (process.env.NODE_ENV !== 'test') {
        middleware.push(logger);
    }

    const enhancer = applyMiddleware(...middleware);

    // Create Store
    const store = createStore<AppState, any, any, any>(rootReducer, initialState, enhancer);

    if (module.hot) {
        module.hot.accept(
            'data/reducers',
            // eslint-disable-next-line global-require
            () => store.replaceReducer(require('data/reducers').default)
        );
    }

    return store;
};

export default { configureStore, history };
