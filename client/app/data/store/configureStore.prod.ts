import { createStore, applyMiddleware, Store } from 'redux';
import thunk from 'redux-thunk';
import { createBrowserHistory } from 'history';
import createRootReducer, { AppState, initialAppState } from 'data/reducers';

const history = createBrowserHistory();
const rootReducer = createRootReducer(history);
const enhancer = applyMiddleware(thunk);

const configureStore = (initialState: AppState = initialAppState): Store<AppState> => createStore<AppState, any, any, any>(rootReducer, initialState, enhancer);

export default { configureStore, history };
