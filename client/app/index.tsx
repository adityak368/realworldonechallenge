import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { store, history } from 'data/store/configureStore';

import intl from 'common/intl';

import enUS from 'res/locales/en-US';

import AuthManager from 'common/authmanager';

import Logger from 'common/logger';
import Broker from 'lib/comm/messaging/broker';
import config from 'app/config';
import ContactManager from 'common/contactmanager';

// locale data
const locales = {
    'en-US': enUS
};

const initApp = async () => {
    try {
        intl.init({
            currentLocale: 'en-US',
            locales
        });
        window.broker = new Broker({
            host: config.brokerHost,
            port: config.brokerPort
        });
        window.authManager = new AuthManager();
        window.contactManager = new ContactManager();

        const App = require('containers/App').default;
        render(
            <AppContainer>
                <App store={store} history={history} />
            </AppContainer>,
            document.getElementById('root')
        );

        if (module.hot) {
            module.hot.accept('containers/App', () => {
                // eslint-disable-next-line global-require
                const NextApp = require('containers/App').default;
                render(
                    <AppContainer>
                        <NextApp store={store} history={history} />
                    </AppContainer>,
                    document.getElementById('root')
                );
            });
        }
    } catch (err) {
        Logger.error(err);
    }
};

initApp();
