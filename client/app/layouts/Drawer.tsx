import React, { Component } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import ROUTES_ITEMS from 'routing/routedefs';

import styles from 'styles/Header';
import withStyles, { WithStyles } from 'react-jss';
import { Icon, Menu, Drawer } from 'antd';
import Logo from 'components/Logo';

interface Props {
    drawerIsOpen: boolean;
    changeDrawerState: (drawerIsOpen: boolean) => void;
}
type StyledProps = WithStyles<typeof styles> & Props;

interface IState {
    selectedItem: string;
}

class DrawerWrapper extends Component<
    RouteComponentProps<StyledProps>,
    IState
> {
    state = {
        selectedItem: window.location.pathname
    };

    onMenuItemClicked = (path: string, item: string) => {
        this.setState({ selectedItem: item });
        const { history } = this.props;
        history.push(path);
    };

    render() {
        const { classes, drawerIsOpen, changeDrawerState } = this.props;
        const { selectedItem } = this.state;

        const menuItems = [];
        ROUTES_ITEMS.forEach(item => {
            if (item.isInSideMenu) {
                menuItems.push(
                    <Menu.Item
                        key={item.path}
                        onClick={() =>
                            this.onMenuItemClicked(item.path, item.path)
                        }
                    >
                        <Icon type={item.icon} />
                        <span>{item.text}</span>
                    </Menu.Item>
                );
            }
        });

        return (
            <Drawer
                title={<Logo className={classes.logo} />}
                placement="right"
                closable={true}
                onClose={() => changeDrawerState(false)}
                visible={drawerIsOpen}
                bodyStyle={{
                    top: 0,
                    left: 0,
                    overflow: 'auto',
                    height: '100vh',
                    transition: '0.2s',
                    padding: 0
                }}
            >
                <Menu
                    mode="inline"
                    selectedKeys={[selectedItem]}
                    style={{ height: '100%', borderRight: 0 }}
                >
                    {menuItems}
                </Menu>
            </Drawer>
        );
    }
}

export default withRouter(withStyles(styles)(DrawerWrapper));
