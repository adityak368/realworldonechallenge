import React from 'react';

import intl from 'common/intl';

import styles from 'styles/Footer';
import withStyles, { WithStyles } from 'react-jss';

interface Props {}
type StyledProps = WithStyles<typeof styles> & Props;

const Footer: React.FC<StyledProps> = ({ classes }) => (
    <footer id="footer" className={classes.footer}>
        <span className={classes.footerItem} style={{ marginLeft: 'auto' }}>
            Copyright © Test
        </span>
    </footer>
);

export default withStyles(styles)(Footer);
