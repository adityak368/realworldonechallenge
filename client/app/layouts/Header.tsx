import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { If, Then, Else } from 'react-if';
import Notifications from 'components/Notifications';

import intl from 'common/intl';
import { IAuthState } from 'data/reducers/auth';

import { Layout, Menu, Icon, Button, Avatar } from 'antd';

import classNames from 'classnames';

import styles from 'styles/Header';
import withStyles, { WithStyles } from 'react-jss';
import { LANGUAGES } from 'common/types';
import { getName } from 'common/utils';
import { Tooltip } from 'antd';

const { SubMenu } = Menu;

interface Props {
    auth: IAuthState;
    changeSideBarState: (isSideBarOpen: boolean) => void;
    changeDrawerState: (drawerIsOpen: boolean) => void;
    isSideBarOpen: boolean;
    left: Number;
    drawerIsOpen: boolean;
    isMobile: boolean;
    notifications: Array<INotification>;
    clearNotifications: () => void;
    onLogout: () => void;
    onLogin: () => void;
    changeLocale: (locale: string) => void;
}
type StyledProps = WithStyles<typeof styles> & Props;

const Header: React.FC<StyledProps> = ({
    auth,
    classes,
    isSideBarOpen,
    left,
    drawerIsOpen,
    changeDrawerState,
    changeSideBarState,
    isMobile,
    notifications,
    clearNotifications,
    onLogout,
    changeLocale,
    onLogin
}) => {
    const currentLocale = LANGUAGES.find(
        lang => lang.key === intl.getCurrentLocale()
    );
    return (
        <React.Fragment>
            <Layout.Header
                id="header"
                style={{ left }}
                className={classNames('header', classes.header)}
            >
                <Icon
                    className={classes.burgerButton}
                    type={
                        isMobile
                            ? drawerIsOpen
                                ? 'menu-fold'
                                : 'menu-unfold'
                            : isSideBarOpen
                            ? 'menu-fold'
                            : 'menu-unfold'
                    }
                    onClick={() => {
                        if (isMobile) {
                            changeDrawerState(!drawerIsOpen);
                        } else {
                            changeSideBarState(!isSideBarOpen);
                        }
                    }}
                />
                <If condition={auth.isLoggedIn}>
                    <Then>
                        <div className={classes.rightNav}>
                            <Tooltip
                                title={
                                    auth.isConnectedToServer
                                        ? intl.get('ConnectedToServer')
                                        : intl.get('NotConnectedToServer')
                                }
                            >
                                <Icon
                                    style={{
                                        color: auth.isConnectedToServer
                                            ? 'green'
                                            : 'red'
                                    }}
                                    className={classes.burgerButton}
                                    type={
                                        auth.isConnectedToServer
                                            ? 'apartment'
                                            : 'exclamation-circle'
                                    }
                                />
                            </Tooltip>
                            <Notifications
                                onClearNotifications={clearNotifications}
                                notifications={notifications}
                            />
                            <Menu
                                selectable={false}
                                mode="horizontal"
                                className={classes.rightNavItem}
                            >
                                <SubMenu
                                    title={
                                        <Avatar
                                            size="small"
                                            src={
                                                currentLocale
                                                    ? currentLocale.flag
                                                    : ''
                                            }
                                        />
                                    }
                                >
                                    {LANGUAGES.map(item => (
                                        <Menu.Item
                                            key={item.key}
                                            onClick={() =>
                                                changeLocale(item.key)
                                            }
                                        >
                                            <Avatar
                                                size="small"
                                                style={{ marginRight: 8 }}
                                                src={item.flag}
                                            />
                                            {item.title}
                                        </Menu.Item>
                                    ))}
                                </SubMenu>
                            </Menu>
                            <Menu
                                selectable={false}
                                mode="horizontal"
                                className={classes.rightNavItem}
                            >
                                <SubMenu
                                    key="user"
                                    title={
                                        <React.Fragment>
                                            <span>{getName(auth.user)}</span>
                                            <Avatar
                                                style={{ marginLeft: 8 }}
                                                src={'/'}
                                            />
                                        </React.Fragment>
                                    }
                                >
                                    {/* <Menu.Item>
                                        <Link to={'/profile'}>
                                            <span>
                                                <Icon type="user" />
                                                <span>
                                                    {intl.get('Profile')}
                                                </span>
                                            </span>
                                        </Link>
                                    </Menu.Item>
                                    <Menu.Item>
                                        <Link to={'/changepassword'}>
                                            <span>
                                                <Icon type="key" />
                                                <span>
                                                    {intl.get('ChangePassword')}
                                                </span>
                                            </span>
                                        </Link>
                                    </Menu.Item> */}
                                    <Menu.Item>
                                        <Link to="#" onClick={onLogout}>
                                            {
                                                <span>
                                                    <Icon type="logout" />
                                                    <span>
                                                        {intl.get('Logout')}
                                                    </span>
                                                </span>
                                            }
                                        </Link>
                                    </Menu.Item>
                                </SubMenu>
                            </Menu>
                        </div>
                    </Then>
                    <Else>
                        <Button onClick={onLogin} className={classes.rightNav}>
                            {intl.get('Login')}
                        </Button>
                    </Else>
                </If>
            </Layout.Header>
        </React.Fragment>
    );
};

export default withRouter(withStyles(styles)(Header));
