import React from 'react';

import SideBar from 'layouts/SideBar';
import Header from 'containers/Header';
import Footer from 'layouts/Footer';
import Drawer from 'layouts/Drawer';

import styles from 'styles/MainLayout';
import withStyles, { WithStyles } from 'react-jss';
import withSizes from 'react-sizes';
import { Breadcrumbs } from 'react-breadcrumbs';

import { IAuthState } from 'data/reducers/auth';
import authActions from 'data/actions/auth';

import { Layout } from 'antd';

import { If, Then, Else } from 'react-if';

interface Props {
    auth: IAuthState;
    authActions: typeof authActions;
}
type StyledProps = WithStyles<typeof styles> & Props;

interface State {
    isSideBarOpen: boolean;
    drawerIsOpen: boolean;
}

class MainLayout extends React.Component<StyledProps, State> {
    state = {
        isSideBarOpen: false,
        drawerIsOpen: false
    };

    changeSideBarState = (isSideBarOpen: boolean) => {
        this.setState({ isSideBarOpen });
    };

    changeDrawerState = (drawerIsOpen: boolean) => {
        this.setState({ drawerIsOpen });
    };

    render() {
        const {
            auth,
            authActions,
            classes,
            theme,
            isMobile,
            isTablet,
            componentToRender,
            ...rest
        } = this.props;
        const { isSideBarOpen, drawerIsOpen } = this.state;

        let left = isSideBarOpen
            ? theme.sideBarExpandedWidth
            : theme.sideBarCollapsedWidth;
        if (isMobile) {
            left = 0;
        }
        return (
            <Layout hasSider={true}>
                <If condition={!isMobile}>
                    <Then>
                        <SideBar isSideBarOpen={isSideBarOpen} auth={auth} />
                    </Then>
                    <Else>
                        <Drawer
                            drawerIsOpen={drawerIsOpen}
                            auth={auth}
                            changeDrawerState={this.changeDrawerState}
                        />
                    </Else>
                </If>
                <Layout>
                    <Header
                        left={left}
                        changeSideBarState={this.changeSideBarState}
                        changeDrawerState={this.changeDrawerState}
                        isSideBarOpen={isSideBarOpen}
                        drawerIsOpen={drawerIsOpen}
                        isMobile={isMobile}
                    />
                    <Layout.Content
                        style={{
                            marginLeft: left
                        }}
                        className={classes.content}
                    >
                        <div
                            id="contentwrapper"
                            style={{
                                padding: '16px 24px'
                            }}
                        >
                            <Breadcrumbs className={classes.breadcrumbs} />
                            {React.createElement(componentToRender, {
                                auth,
                                authActions,
                                ...rest
                            })}
                        </div>
                        <Footer />
                    </Layout.Content>
                </Layout>
            </Layout>
        );
    }
}
const mapSizesToProps = ({ width }) => ({
    isMobile: width < 576,
    isTablet: width >= 576 && width < 1024,
    isDesktop: width >= 1024
});

export default withSizes(mapSizesToProps)(withStyles(styles)(MainLayout));
