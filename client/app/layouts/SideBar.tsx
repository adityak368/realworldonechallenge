import React, { Component } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import ROUTES_ITEMS from 'routing/routedefs';

import styles from 'styles/Header';
import withStyles, { WithStyles } from 'react-jss';
import { Layout, Icon, Menu } from 'antd';
import Logo from 'components/Logo';

interface Props {
    isSideBarOpen: boolean;
}
type StyledProps = WithStyles<typeof styles> & Props;

interface IState {
    selectedItem: string;
}

class SideBar extends Component<RouteComponentProps<StyledProps>, IState> {
    state = {
        selectedItem: window.location.pathname
    };

    onMenuItemClicked = (path: string, item: string) => {
        this.setState({ selectedItem: item });
        const { history } = this.props;
        history.push(path);
    };

    render() {
        const { classes, theme, isSideBarOpen } = this.props;
        const { selectedItem } = this.state;
        const menuItems = [];
        ROUTES_ITEMS.forEach(item => {
            if (item.isInSideMenu) {
                menuItems.push(
                    <Menu.Item
                        key={item.path}
                        onClick={() =>
                            this.onMenuItemClicked(item.path, item.path)
                        }
                    >
                        <Icon type={item.icon} />
                        <span>{item.text}</span>
                    </Menu.Item>
                );
            }
        });

        return (
            <Layout.Sider
                width={theme.sideBarWidth}
                breakpoint="lg"
                trigger={null}
                collapsible
                className={classes.sidebar}
                collapsed={!isSideBarOpen}
            >
                <Logo className={classes.logo} isSideBarOpen={isSideBarOpen} />
                <Menu
                    selectable
                    selectedKeys={[selectedItem]}
                    theme={theme.name}
                    mode="inline"
                    style={{ height: '100%', borderRight: 0 }}
                >
                    {menuItems}
                </Menu>
            </Layout.Sider>
        );
    }
}

export default withRouter(withStyles(styles)(SideBar));
