export default class Capture {
    getAvailableMediaDevices = async () => {
        const { navigator } = window;
        const mediaIO = {
            audio: new Array<IMediaDevice>(),
            video: new Array<IMediaDevice>(),
            speaker: new Array<IMediaDevice>()
        };
        const deviceInfos: Array<
            MediaDeviceInfo
        > = await navigator.mediaDevices.enumerateDevices();
        deviceInfos.forEach(deviceInfo => {
            switch (deviceInfo.kind) {
                case 'audioinput':
                    {
                        const device = mediaIO.audio.find(
                            d => d.groupId === deviceInfo.groupId
                        );
                        if (device) {
                            return;
                        }
                        mediaIO.audio.push({
                            stream: null,
                            deviceId: deviceInfo.deviceId,
                            label:
                                deviceInfo.label ||
                                `Microphone ${mediaIO.audio.length + 1}`,
                            groupId: deviceInfo.groupId,
                            isEnabled: false
                        });
                    }
                    break;
                case 'videoinput': {
                    const device = mediaIO.video.find(
                        d => d.groupId === deviceInfo.groupId
                    );
                    if (device) {
                        return;
                    }
                    mediaIO.video.push({
                        stream: null,
                        deviceId: deviceInfo.deviceId,
                        label:
                            deviceInfo.label ||
                            `Camera ${mediaIO.video.length + 1}`,
                        groupId: deviceInfo.groupId,
                        isEnabled: false
                    });
                    break;
                }
                case 'audiooutput':
                    {
                        const device = mediaIO.speaker.find(
                            d => d.groupId === deviceInfo.groupId
                        );
                        if (device) {
                            return;
                        }
                        mediaIO.speaker.push({
                            stream: null,
                            deviceId: deviceInfo.deviceId,
                            label:
                                deviceInfo.label ||
                                `Speaker ${mediaIO.speaker.length + 1}`,
                            groupId: deviceInfo.groupId,
                            isEnabled: false
                        });
                    }
                    break;
                default:
                    break;
            }
        });
        return mediaIO;
    };

    getStream = (constraints: MediaStreamConstraints): Promise<MediaStream> => {
        const { navigator } = window;
        return navigator.mediaDevices.getUserMedia(constraints);
    };
    /*
    checkPermission = async (device: string) => {
        const { navigator } = window;
        try {
            const permission: PermissionStatus = await navigator.permissions.query({ name: device });
            if (permission.state === 'denied') {
                switch (device) {
                    case 'camera':
                        break;
                    case 'microphone':
                        break;
                    default:
                        break;
                }
            }
            return permission.state !== 'denied';
        } catch (err) {
            Logger.error(err);
        }
        return false;
    }
*/

    createNewMediaStream = () => {
        const mediaStream = new MediaStream();
        return mediaStream;
    };

    makeAudioOnlyStreamFromExistingStream = (stream: MediaStream) => {
        const audioStream: MediaStream = stream.clone();
        const videoTracks: Array<
            MediaStreamTrack
        > = audioStream.getVideoTracks();
        videoTracks.forEach(track => {
            audioStream.removeTrack(track);
        });
        return audioStream;
    };

    makeVideoOnlyStreamFromExistingStream = (stream: MediaStream) => {
        const videoStream: MediaStream = stream.clone();
        const audioTracks: Array<
            MediaStreamTrack
        > = videoStream.getAudioTracks();
        audioTracks.forEach(track => {
            videoStream.removeTrack(track);
        });
        return videoStream;
    };

    getDefaultDeviceForMediaType = async (mediaType: string) => {
        const { navigator } = window;
        const deviceInfos: Array<
            MediaDeviceInfo
        > = await navigator.mediaDevices.enumerateDevices();
        const defaultDevice: MediaDeviceInfo = deviceInfos
            .filter(deviceInfo => deviceInfo.kind === mediaType)
            .shift();
        if (defaultDevice) {
            return {
                stream: null,
                deviceId: defaultDevice.deviceId,
                label: defaultDevice.label || 'Default Device',
                groupId: defaultDevice.groupId,
                isEnabled: false
            };
        }
        return null;
    };
}
