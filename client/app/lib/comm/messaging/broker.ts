import Centrifuge from 'centrifuge';

import { Namespaces, BrokerSignals } from 'lib/comm/messaging/types';

import notification from 'common/notification';
import EventEmitter from 'events';
import Logger from 'common/logger';
import intl from 'common/intl';

export default class Broker extends EventEmitter {
    options: any = {};

    isConnected: boolean = false;

    conn: Centrifuge = null;

    subscriptionHandle: any = null;

    userStatusHandle: any = null;

    credentials: any = null;

    constructor(options: any) {
        super();
        this.options = options;
    }

    connect = (credentials: any) => {
        const centrifuge = new Centrifuge(
            `wss://${this.options.host}/connection/websocket`
        );
        centrifuge.setToken(credentials.authToken);
        centrifuge.connect();
        centrifuge.on('connect', context => {
            Logger.info('Broker Connection Established');
            this.isConnected = true;
            this.emit(BrokerSignals.ConnectedToServer);
        });
        centrifuge.on('disconnect', context => {
            notification.error({
                message: `Disconnected from Server: ${context.reason}`
            });
            this.isConnected = false;
            this.emit('disconnect');
        });
        centrifuge.on('error', errContext => {
            notification.error({
                message: `Server Connection Error ${errContext.error}`
            });
            this.emit('error');
        });
        this.credentials = credentials;
        this.conn = centrifuge;
    };

    listenForCommands = () => {
        const subscription = this.conn
            .subscribe(
                `${Namespaces.User}${this.credentials.username}`,
                message => {
                    const { name, data } = message.data;
                    if (!name) {
                        return;
                    }
                    this.emit(name, data);
                }
            )
            .on('error', errContext => {
                notification.error({
                    message: `Error communicating with server: ${
                        errContext.message
                    }`
                });
            })
            .on('subscribe', context => {
                Logger.info('Listening for broker events');
            })
            .on('unsubscribe', context => {
                Logger.info('Lost Connection with broker');
            });

        this.subscriptionHandle = subscription;
    };

    listenForContactStatus = () => {
        const subscription = this.conn
            .subscribe('status', message => {
                console.log(message);
            })
            .on('error', errContext => {
                notification.error({
                    message: `Error communicating with server: ${
                        errContext.message
                    }`
                });
            })
            .on('subscribe', context => {
                Logger.info('Listening for user status');
            })
            .on('unsubscribe', context => {
                Logger.info('Lost Connection with users');
            })
            .on('join', message => {
                this.emit(BrokerSignals.PeerJoined, message.info.conn_info);
            })
            .on('leave', message => {
                this.emit(BrokerSignals.PeerLeft, message.info.conn_info);
            });

        this.userStatusHandle = subscription;
    };

    publishToSession = (sessionID: string, data: any) => {
        // because we get relayed back the message we published. So ignore it
        const sender = this.credentials.username;
        const wrappedData = { ...data, sender };
        if (this.conn) {
            return this.conn.publish(sessionID, wrappedData);
        }
        return Promise.reject(new Error(intl.get('NotConnectedToServer')));
    };

    getUserPresence = () => {
        return this.userStatusHandle.presence();
    };
}
