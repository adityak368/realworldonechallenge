export const Namespaces = {
    User: 'user'
};

export const enum BrokerSignals {
    ConnectedToServer = 'ConnectedToServer',
    ChatMessage = 'ChatMessage',
    Notification = 'Notification',
    Disconnect = 'disconnect',
    PeerJoined = 'PeerJoined',
    PeerLeft = 'PeerLeft',
    UpdateAudioVideoState = 'UpdateAudioVideoState',
    Handshake = 'Handshake'
}

export interface BrokerSignal {
    name: string;
    data: any;
}

export const GenerateBrokerSignal = (
    name: string,
    data: any
): BrokerSignal => ({
    name,
    data
});

export interface Handshake {
    handshakeRequester: string;
    connData: any;
}
