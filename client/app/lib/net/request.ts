import Logger from 'common/logger';
import intl from 'common/intl';

export class Request {
    url: string = '';

    constructor(url: string) {
        this.url = url;
    }

    post = (data: any, options?: any) => {
        const headers: any = this.generateHeaders(options);

        return new Promise((resolve, reject) => {
            fetch(this.url, {
                method: 'POST',
                headers,
                body: JSON.stringify(data)
            })
                .then(response => {
                    if (!response.ok) {
                        throw response;
                    }
                    return response.json();
                })
                .then(json => resolve(json))
                .catch(err => {
                    if (typeof err.json === 'function') {
                        err.json()
                            .then(jsonErr => reject(jsonErr))
                            .catch(err => {
                                Logger.error(err);
                                reject(
                                    new Error(intl.get('CouldNotFetchData'))
                                );
                            });
                        return;
                    }
                    Logger.error(err);
                    reject(new Error(intl.get('CouldNotFetchData')));
                });
        });
    };

    get = (options?: any) => {
        return new Promise((resolve, reject) => {
            const headers: any = this.generateHeaders(options);
            fetch(this.url, {
                method: 'GET',
                headers
            })
                .then(response => {
                    if (!response.ok) {
                        throw response;
                    }
                    return response.json();
                })
                .then(json => resolve(json))
                .catch(err => {
                    if (typeof err.json === 'function') {
                        err.json()
                            .then(jsonErr => reject(jsonErr))
                            .catch(err => {
                                Logger.error(err);
                                reject(
                                    new Error(intl.get('CouldNotFetchData'))
                                );
                            });
                        return;
                    }
                    Logger.error(err);
                    reject(new Error(intl.get('CouldNotFetchData')));
                });
        });
    };

    put = (data: any, options?: any) => {
        const headers: any = this.generateHeaders(options);
        return new Promise((resolve, reject) => {
            fetch(this.url, {
                method: 'PUT',
                headers,
                body: JSON.stringify(data)
            })
                .then(response => {
                    if (!response.ok) {
                        throw response;
                    }
                    return response.json();
                })
                .then(json => resolve(json))
                .catch(err => {
                    if (typeof err.json === 'function') {
                        err.json()
                            .then(jsonErr => reject(jsonErr))
                            .catch(err => {
                                Logger.error(err);
                                reject(
                                    new Error(intl.get('CouldNotFetchData'))
                                );
                            });
                        return;
                    }
                    Logger.error(err);
                    reject(new Error(intl.get('CouldNotFetchData')));
                });
        });
    };

    delete = (options?: any) => {
        const headers: any = this.generateHeaders(options);
        return new Promise((resolve, reject) => {
            fetch(this.url, {
                method: 'DELETE',
                headers
            })
                .then(response => {
                    if (!response.ok) {
                        throw response;
                    }
                    return response.json();
                })
                .then(json => resolve(json))
                .catch(err => {
                    if (typeof err.json === 'function') {
                        err.json()
                            .then(jsonErr => reject(jsonErr))
                            .catch(err => {
                                Logger.error(err);
                                reject(
                                    new Error(intl.get('CouldNotFetchData'))
                                );
                            });
                        return;
                    }
                    Logger.error(err);
                    reject(new Error(intl.get('CouldNotFetchData')));
                });
        });
    };

    generateHeaders = (options: any) => {
        const authToken: string = window.localStorage.getItem('authToken');
        let requestHeaders: any = { 'Content-Type': 'application/json' };

        if (authToken) {
            requestHeaders = {
                ...requestHeaders,
                Authorization: `Bearer ${authToken}`
            };
        }

        if (options && options.headers) {
            requestHeaders = { ...requestHeaders, ...options.headers };
        }

        return requestHeaders;
    };
}
