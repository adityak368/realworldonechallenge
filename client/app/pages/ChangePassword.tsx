import React from 'react';
import intl from 'common/intl';
import DocumentTitle from 'react-document-title';

import styles from 'styles/Login';
import withStyles, { WithStyles } from 'react-jss';

import { Formik, Form, Field, FormikActions } from 'formik';
import { changePasswordSchema, ChangePasswordForm } from 'common/form';
import { AntInput } from 'components/formik';
import Logo from 'components/Logo';

import { Row, Col, Typography, Button, Card, Icon } from 'antd';

import { IAuthState } from 'data/reducers/auth';
import authActions from 'data/actions/auth';

const { Title } = Typography;

interface Props extends WithStyles<typeof styles> {
    onChangePassword: (
        values: ChangePasswordForm,
        actions: FormikActions<ChangePasswordForm>
    ) => void;
    auth: IAuthState;
    authActions: typeof authActions;
}
type StyledProps = WithStyles<typeof styles> & Props;

const ChangePassword: React.FC<StyledProps> = ({
    classes,
    onChangePassword
}) => (
    <React.Fragment>
        <Row
            gutter={16}
            align="middle"
            justify="space-around"
            type="flex"
            className="container"
        >
            <Col
                xs={{ span: 18 }}
                md={{ span: 12 }}
                lg={{ span: 6 }}
                style={{ textAlign: 'center' }}
            >
                <Card>
                    <Logo className={classes.logo} />
                    <Title level={4}>{intl.get('ChangePassword')}</Title>
                    <Row>
                        <Formik
                            initialValues={{
                                oldpassword: '',
                                newpassword: '',
                                confirmnewpassword: ''
                            }}
                            validationSchema={changePasswordSchema}
                            onSubmit={onChangePassword}
                        >
                            {({ isSubmitting }) => (
                                <Form>
                                    <Field
                                        required
                                        name="oldpassword"
                                        type="password"
                                        prefix={
                                            <Icon
                                                type="key"
                                                style={{
                                                    color: 'rgba(0,0,0,.25)'
                                                }}
                                            />
                                        }
                                        placeholder={intl.get('OldPassword')}
                                        component={AntInput}
                                    />
                                    <Field
                                        required
                                        placeholder={intl.get('NewPassword')}
                                        name="newpassword"
                                        prefix={
                                            <Icon
                                                type="key"
                                                style={{
                                                    color: 'rgba(0,0,0,.25)'
                                                }}
                                            />
                                        }
                                        type="password"
                                        component={AntInput}
                                    />
                                    <Field
                                        required
                                        placeholder={intl.get(
                                            'ConfirmNewPassword'
                                        )}
                                        name="confirmnewpassword"
                                        prefix={
                                            <Icon
                                                type="key"
                                                style={{
                                                    color: 'rgba(0,0,0,.25)'
                                                }}
                                            />
                                        }
                                        type="password"
                                        component={AntInput}
                                    />
                                    <Button
                                        type="primary"
                                        htmlType="submit"
                                        disabled={isSubmitting}
                                        className={classes.submit}
                                    >
                                        {intl.get('ChangePassword')}
                                    </Button>
                                </Form>
                            )}
                        </Formik>
                    </Row>
                </Card>
            </Col>
        </Row>
        <DocumentTitle title={intl.get('ChangePassword')} />
    </React.Fragment>
);

export default withStyles(styles)(ChangePassword);
