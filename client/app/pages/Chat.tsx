import React from 'react';
import intl from 'common/intl';
import DocumentTitle from 'react-document-title';

import EmojiPicker from 'emoji-picker-react';

import {
    Row,
    Col,
    Typography,
    Button,
    Card,
    Select,
    Tooltip,
    List,
    Avatar,
    Input,
    Icon,
    Popover
} from 'antd';

import styles from 'styles/Profile';
import withStyles, { WithStyles } from 'react-jss';

interface Props {
    contacts: Array<IContact>;
    selectedVideoDevice: IMediaDevice;
    selectedContact: string;
    onSendChatMsg: (user: string, msg: string) => void;
    onVideoShare: (user: string) => void;
}
type StyledProps = WithStyles<typeof styles> & Props;

const Chat: React.FC<StyledProps> = ({
    selectedContact,
    onSendChatMsg,
    contacts,
    selectedVideoDevice,
    onVideoShare
}: Props) => {
    const contact: IContact = contacts.find(
        c => c.username === selectedContact
    );
    const [isEmojiPickerVisible, setIsEmojiPickerVisible] = React.useState(
        false
    );
    const [message, setMessage] = React.useState('');
    return (
        <React.Fragment>
            <Card
                bordered={false}
                bodyStyle={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'flex-start',
                    height: 500
                }}
            >
                <div
                    style={{
                        display: 'flex'
                    }}
                >
                    <Tooltip title={intl.get('ShareVideo')}>
                        <Button
                            onClick={() => onVideoShare(selectedContact)}
                            style={{ marginRight: 20 }}
                            icon={
                                selectedVideoDevice !== null &&
                                selectedVideoDevice.isEnabled
                                    ? 'video-camera'
                                    : 'pause'
                            }
                        />
                    </Tooltip>
                    <Typography.Title level={4}>
                        {`${intl.get('ChatWith')}  ${selectedContact}`}
                    </Typography.Title>
                </div>
                <List
                    style={{ overflowY: 'scroll', marginTop: 'auto' }}
                    itemLayout="horizontal"
                    dataSource={contact ? contact.msg : []}
                    renderItem={(item: ChatMsg) => (
                        <List.Item>
                            <List.Item.Meta
                                avatar={
                                    <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                                }
                                title={item.from}
                                description={item.text}
                            />
                        </List.Item>
                    )}
                />
                <div
                    style={{
                        display: 'flex'
                    }}
                >
                    <Input
                        value={message}
                        onChange={e => setMessage(e.target.value)}
                        defaultValue=""
                        placeholder={intl.get('SendMessage')}
                        style={{
                            marginRight: 10
                        }}
                        onPressEnter={() => {
                            setMessage('');
                            onSendChatMsg(selectedContact, message);
                        }}
                    />
                    <Button
                        onClick={() => {
                            setMessage('');
                            onSendChatMsg(selectedContact, message);
                        }}
                        icon="message"
                    />
                    <Popover
                        content={
                            <EmojiPicker
                                onEmojiClick={emoji => {
                                    setMessage(
                                        message +
                                            String.fromCodePoint(
                                                parseInt(emoji, 16)
                                            )
                                    );
                                }}
                            />
                        }
                        title="Select Emoji"
                        trigger="click"
                        visible={isEmojiPickerVisible}
                        onVisibleChange={() => {
                            setIsEmojiPickerVisible(!isEmojiPickerVisible);
                        }}
                    >
                        <Button
                            style={{ marginLeft: 10 }}
                            icon="meh"
                            type="primary"
                        />
                    </Popover>
                </div>
            </Card>
            <DocumentTitle title={intl.get('Profile')} />
        </React.Fragment>
    );
};

export default withStyles(styles)(Chat);
