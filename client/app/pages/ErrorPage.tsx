import React from 'react';
import intl from 'common/intl';

import { Layout, Result, Button } from 'antd';

interface Props {
    retry: () => void;
}

const ErrorPage: React.FC<Props> = ({ retry }) => (
	<Layout
		style={{
            height: '100vh'
        }}
	>
        <Result
            status="500"
            title={intl.get('Error')}
            subTitle={intl.get('SomethingWentWrong')}
            extra={
                <Button type="primary" onClick={retry}>
                    {intl.get('Restart')}
                </Button>
            }
        />
    </Layout>
);

export default ErrorPage;
