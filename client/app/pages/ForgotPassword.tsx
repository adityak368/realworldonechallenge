import React from 'react';
import { Link } from 'react-router-dom';
import intl from 'common/intl';
import DocumentTitle from 'react-document-title';
import classNames from 'classnames';

import styles from 'styles/Login';
import withStyles, { WithStyles } from 'react-jss';

import { Formik, Form, Field, FormikActions } from 'formik';
import { ForgotPasswordForm, forgotPasswordSchema } from 'common/form';
import { AntInput } from 'components/formik';
import Logo from 'components/Logo';

import { Row, Col, Typography, Button, Card, Icon } from 'antd';

import { IAuthState } from 'data/reducers/auth';
import authActions from 'data/actions/auth';

const { Title, Text } = Typography;

interface Props extends WithStyles<typeof styles> {
    onSubmit: (
        values: ForgotPasswordForm,
        actions: FormikActions<ForgotPasswordForm>
    ) => void;
    auth: IAuthState;
    authActions: typeof authActions;
}
type StyledProps = WithStyles<typeof styles> & Props;

const ForgotPassword: React.FC<StyledProps> = ({ classes, onSubmit }) => (
    <React.Fragment>
        <Row
            gutter={16}
            align="middle"
            justify="space-around"
            type="flex"
            className="container"
        >
            <Col
                xs={{ span: 18 }}
                md={{ span: 12 }}
                lg={{ span: 6 }}
                style={{ textAlign: 'center' }}
            >
                <Card>
                    <Logo className={classes.logo} />
                    <Title level={4}>{intl.get('ForgotPassword')}</Title>
                    <Row>
                        <Formik
                            initialValues={{
                                email: ''
                            }}
                            validationSchema={forgotPasswordSchema}
                            onSubmit={onSubmit}
                        >
                            {({ isSubmitting }) => (
                                <Form>
                                    <Field
                                        required
                                        name="email"
                                        type="text"
                                        prefix={
                                            <Icon
                                                type="user"
                                                style={{
                                                    color: 'rgba(0,0,0,.25)'
                                                }}
                                            />
                                        }
                                        placeholder={intl.get('EmailId')}
                                        component={AntInput}
                                    />
                                    <Button
                                        type="primary"
                                        htmlType="submit"
                                        disabled={isSubmitting}
                                        className={classes.submit}
                                    >
                                        {intl.get('Reset')}
                                    </Button>
                                </Form>
                            )}
                        </Formik>
                        <Link
                            to="/signup"
                            className={classNames(
                                'link-underline',
                                classes.signUpLink
                            )}
                        >
                            <Text type="secondary">{intl.get('SignUp')}</Text>
                        </Link>
                    </Row>
                    <Link
                        to="/login"
                        className={classNames(
                            'link-underline',
                            classes.forgotPasswordLink
                        )}
                    >
                        <Text type="secondary">{intl.get('Login')}</Text>
                    </Link>
                </Card>
            </Col>
        </Row>
        <DocumentTitle title={intl.get('ForgotPassword')} />
    </React.Fragment>
);

export default withStyles(styles)(ForgotPassword);
