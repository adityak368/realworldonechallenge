import React from 'react';
import { Link } from 'react-router-dom';
import intl from 'common/intl';
import DocumentTitle from 'react-document-title';
import { Row, List, Avatar, Col, Divider, Typography } from 'antd';

import { IAuthState } from 'data/reducers/auth';
import Chat from 'pages/Chat';
import UserDisplay from 'components/UserDisplay';
import { MediaType } from 'common/types';
import { IContactState } from 'data/reducers/contacts';

interface Props {
    auth: IAuthState;
    contacts: IContactState;
    getPeerStreamOfType: (user: string, mediaType: MediaType) => MediaStream;
    onSendChatMsg: (user: string, msg: string) => void;
    onVideoShare: (user: string) => void;
}

const Home: React.FC<Props> = ({
    contacts,
    auth,
    onSendChatMsg,
    getPeerStreamOfType,
    onVideoShare
}: Props) => {
    const [selectedContact, setSelectedContact] = React.useState('');
    const onlineContacts = contacts.contacts.filter(
        (contact: IContact) => contact.username !== auth.user.username
    );
    return (
        <React.Fragment>
            <Row
                type="flex"
                className="container"
                style={{
                    flexDirection: 'column',
                    position: 'relative',
                    height: '100vh'
                }}
            >
                <Col
                    xs={6}
                    style={{
                        padding: 10
                    }}
                >
                    <Typography.Title
                        level={4}
                        style={{
                            margin: 10
                        }}
                    >
                        {intl.get('OnlineUsers')}
                    </Typography.Title>
                    <List
                        itemLayout="horizontal"
                        dataSource={onlineContacts}
                        renderItem={(item: IContact) => (
                            <List.Item>
                                <List.Item.Meta
                                    avatar={
                                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                                    }
                                    title={
                                        <Link
                                            to={'#'}
                                            onClick={() => {
                                                setSelectedContact(
                                                    item.username
                                                );
                                            }}
                                        >
                                            {item.username}
                                        </Link>
                                    }
                                    description={`Email: ${item.email}, Name: ${
                                        item.name
                                    }`}
                                />
                            </List.Item>
                        )}
                    />
                </Col>
                <Divider
                    type="vertical"
                    style={{
                        height: '100%',
                        margin: 0
                    }}
                />
                <Col
                    xs={17}
                    style={{
                        padding: 0
                    }}
                >
                    <Chat
                        onSendChatMsg={onSendChatMsg}
                        selectedContact={selectedContact}
                        contacts={onlineContacts}
                        onVideoShare={onVideoShare}
                        selectedVideoDevice={contacts.media[MediaType.Video]}
                    />
                    <UserDisplay
                        selectedContact={selectedContact}
                        getPeerStreamOfType={getPeerStreamOfType}
                        contacts={contacts}
                    />
                </Col>
            </Row>
            <DocumentTitle title={intl.get('AppName')} />
        </React.Fragment>
    );
};

export default Home;
