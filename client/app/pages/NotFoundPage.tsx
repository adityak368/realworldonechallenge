import React from 'react';
import intl from 'common/intl';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import { Row, Typography, Button } from 'antd';

import DocumentTitle from 'react-document-title';

import { IAuthState } from 'data/reducers/auth';
import authActions from 'data/actions/auth';

interface Props {
    auth: IAuthState;
    authActions: typeof authActions;
}

const NotFoundPage: React.FC<RouteComponentProps<Props>> = ({ history }) => (
    <React.Fragment>
        <Row
            type="flex"
            className="container"
            style={{
                position: 'relative'
            }}
        >
            <div
                style={{
                    position: 'absolute',
                    top: '40%',
                    left: '70%',
                    zIndex: 10
                }}
            >
                <Typography.Title level={1}>404</Typography.Title>
                <Typography.Title level={4}>
                    {intl.get('NotFoundPage')}
                </Typography.Title>
                <Button type="primary" onClick={() => history.goBack()}>
                    {intl.get('GoBack')}
                </Button>
            </div>
            <div
                style={{
                    width: '100%',
                    height: '100%',
                    background:
                        'url(https://gw.alipayobjects.com/zos/rmsportal/KpnpchXsobRgLElEozzI.svg) no-repeat center'
                }}
            />
        </Row>
        <DocumentTitle title={intl.get('NotFoundPage')} />
    </React.Fragment>
);

export default withRouter(NotFoundPage);
