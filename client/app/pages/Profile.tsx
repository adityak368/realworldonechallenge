import React from 'react';
import intl from 'common/intl';
import DocumentTitle from 'react-document-title';

import { profileSchema, ProfileForm } from 'common/form';
import { IAuthState } from 'data/reducers/auth';

import { Row, Col, Typography, Button, Card, Select } from 'antd';

import { Formik, Form, Field, FormikActions } from 'formik';
import { AntInput, AntSelect, AntTextArea } from 'components/formik';
import ProfileCard from 'components/ProfileCard';

import styles from 'styles/Profile';
import withStyles, { WithStyles } from 'react-jss';

interface Props {
    auth: IAuthState;
    onSubmit: (
        values: ProfileForm,
        actions: FormikActions<ProfileForm>
    ) => void;
}
type StyledProps = WithStyles<typeof styles> & Props;

const Profile: React.FC<StyledProps> = ({ auth, onSubmit, classes }) => (
    <Row gutter={16} justify="space-around">
        <Col xs={24} sm={8}>
            <Card style={{ minHeight: '80vh' }}>
                <ProfileCard user={auth.user} />
            </Card>
        </Col>
        <Col xs={24} sm={16}>
            <Card>
                <Typography.Title level={4}>
                    {intl.get('EditProfile')}
                </Typography.Title>
                <Formik
                    initialValues={{
                        firstname: auth.user.FirstName || '',
                        lastname: auth.user.LastName || '',
                        contact: auth.user.Contact || '',
                        headline: auth.user.Headline || '',
                        address: auth.user.Address || ''
                    }}
                    validationSchema={profileSchema}
                    onSubmit={onSubmit}
                >
                    {({ isSubmitting }) => (
                        <Form>
                            <Row gutter={16} justify="space-around">
                                <Col
                                    xs={{ span: 24 }}
                                    md={{ span: 12 }}
                                    lg={{ span: 12 }}
                                    style={{ textAlign: 'center' }}
                                >
                                    <Field
                                        type="text"
                                        required
                                        placeholder={intl.get('FirstName')}
                                        name="firstname"
                                        component={AntInput}
                                    />
                                    <Field
                                        type="text"
                                        required
                                        placeholder={intl.get('LastName')}
                                        name="lastname"
                                        component={AntInput}
                                    />
                                    <Field
                                        formWrapperClassName={classes.contact}
                                        type="text"
                                        placeholder={intl.get('Contact')}
                                        name="contact"
                                        component={AntInput}
                                    />
                                </Col>
                                <Col
                                    xs={{ span: 24 }}
                                    md={{ span: 12 }}
                                    lg={{ span: 12 }}
                                    style={{ textAlign: 'center' }}
                                >
                                    <Field
                                        type="text"
                                        placeholder={intl.get('ProfileSummary')}
                                        name="headline"
                                        component={AntInput}
                                    />
                                    <Field
                                        placeholder={intl.get('Address')}
                                        name="address"
                                        component={AntTextArea}
                                    />
                                </Col>
                            </Row>
                            <Button
                                type="primary"
                                htmlType="submit"
                                disabled={isSubmitting}
                                className={classes.submit}
                            >
                                {intl.get('Save')}
                            </Button>
                        </Form>
                    )}
                </Formik>
            </Card>
        </Col>
        <DocumentTitle title={intl.get('Profile')} />
    </Row>
);

export default withStyles(styles)(Profile);
