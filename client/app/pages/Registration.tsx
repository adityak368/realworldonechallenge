import React from 'react';

import { Link } from 'react-router-dom';
import intl from 'common/intl';
import { Formik, Form, Field, FormikActions } from 'formik';
import { RegistrationForm, registrationFormSchema } from 'common/form';
import DocumentTitle from 'react-document-title';
import classNames from 'classnames';

import styles from 'styles/Registration';
import withStyles, { WithStyles } from 'react-jss';
import { AntInput } from 'components/formik';

import { Row, Col, Typography, Button, Card } from 'antd';

import { IAuthState } from 'data/reducers/auth';
import authActions from 'data/actions/auth';
import Logo from 'components/Logo';

const { Title, Text } = Typography;

interface Props {
    onSubmit: (
        values: RegistrationForm,
        actions: FormikActions<RegistrationForm>
    ) => void;
    auth: IAuthState;
    authActions: typeof authActions;
}
type StyledProps = WithStyles<typeof styles> & Props;

const Registration: React.FC<StyledProps> = ({ classes, onSubmit }) => (
    <React.Fragment>
        <Row
            gutter={16}
            align="middle"
            justify="space-around"
            type="flex"
            className="container"
        >
            <Col
                xs={{ span: 18 }}
                md={{ span: 12 }}
                lg={{ span: 6 }}
                style={{ textAlign: 'center' }}
            >
                <Card>
                    <Logo className={classes.logo} />
                    <Title level={3}>{intl.get('CandidateSignUp')}</Title>
                    <Formik
                        initialValues={{
                            email: '',
                            password: '',
                            confirmpassword: '',
                            name: '',
                            username: ''
                        }}
                        validationSchema={registrationFormSchema}
                        onSubmit={onSubmit}
                    >
                        {({ isSubmitting }) => (
                            <Form>
                                <Field
                                    required
                                    placeholder={intl.get('EmailId')}
                                    name="email"
                                    type="text"
                                    autoComplete="email"
                                    component={AntInput}
                                />
                                <Field
                                    required
                                    placeholder={intl.get('UserName')}
                                    name="username"
                                    type="text"
                                    component={AntInput}
                                />
                                <Field
                                    required
                                    placeholder={intl.get('Password')}
                                    name="password"
                                    type="password"
                                    component={AntInput}
                                />
                                <Field
                                    required
                                    placeholder={intl.get('ConfirmPassword')}
                                    name="confirmpassword"
                                    type="password"
                                    component={AntInput}
                                />
                                <Field
                                    required
                                    placeholder={intl.get('Name')}
                                    name="name"
                                    type="text"
                                    component={AntInput}
                                />
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                    disabled={isSubmitting}
                                    className={classes.submit}
                                >
                                    {intl.get('SignUp')}
                                </Button>
                            </Form>
                        )}
                    </Formik>
                    <Link
                        to="/login"
                        className={classNames(
                            'link-underline',
                            classes.loginLink
                        )}
                    >
                        <Text type="secondary">{intl.get('Login')}</Text>
                    </Link>
                </Card>
            </Col>
        </Row>
        <DocumentTitle title={intl.get('SignUp')} />
    </React.Fragment>
);

export default withStyles(styles)(Registration);
