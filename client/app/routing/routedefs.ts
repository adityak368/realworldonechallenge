import intl from 'common/intl';

import ProfileContainer from 'containers/Profile';
import NotFoundPage from 'pages/NotFoundPage';
import LoginContainer from 'containers/Login';
import Registration from 'containers/Registration';
import Home from 'containers/Home';
import ForgotPassword from 'containers/ForgotPassword';
import ChangePassword from 'containers/ChangePassword';
import ResetPassword from 'containers/ResetPassword';

export default [
    {
        path: '/',
        text: intl.get('Dashboard'),
        icon: 'home',
        exact: true,
        componentToRender: Home,
        isProtected: true,
        isInSideMenu: false,
        isInTopMenu: false,
        isNewPage: false
    },
    {
        path: '/signup',
        text: intl.get('Register'),
        icon: 'user-add',
        exact: true,
        componentToRender: Registration,
        isProtected: false,
        isInSideMenu: false,
        isInTopMenu: false,
        isNewPage: true
    },
    //TODO: REMOVE
    // {
    //     path: '/profile',
    //     text: intl.get('Profile'),
    //     icon: 'user',
    //     exact: true,
    //     componentToRender: ProfileContainer,
    //     isProtected: true,
    //     isInSideMenu: false,
    //     isInTopMenu: false,
    //     isNewPage: false
    // },
    {
        path: '/login',
        text: intl.get('Login'),
        icon: 'user',
        exact: true,
        componentToRender: LoginContainer,
        isProtected: false,
        isInSideMenu: false,
        isInTopMenu: false,
        isNewPage: true
    },
    {
        path: '/forgotpassword',
        text: intl.get('ForgotPassword'),
        icon: 'help',
        exact: true,
        componentToRender: ForgotPassword,
        isProtected: false,
        isInSideMenu: false,
        isInTopMenu: false,
        isNewPage: true
    },
    //TODO: REMOVE
    // {
    //     path: '/changepassword',
    //     text: intl.get('ChangePassword'),
    //     icon: 'key',
    //     exact: true,
    //     componentToRender: ChangePassword,
    //     isProtected: true,
    //     isInSideMenu: false,
    //     isInTopMenu: false,
    //     isNewPage: false
    // },
    {
        path: '/reset',
        text: intl.get('ResetPassword'),
        icon: 'help',
        exact: true,
        componentToRender: ResetPassword,
        isProtected: false,
        isInSideMenu: false,
        isInTopMenu: false,
        isNewPage: true
    },
    {
        path: '/*',
        text: intl.get('NotFoundPage'),
        icon: '',
        exact: false,
        componentToRender: NotFoundPage,
        isProtected: false,
        isInSideMenu: false,
        isInTopMenu: false,
        isNewPage: false
    }
];
