const styles = theme => ({
    footer: {
        backgroundColor: theme.headerColor,
        padding: 0,
        paddingRight: 20,
        margin: 0,
        marginTop: 20,
        boxShadow: '0px -2px 0px rgba(229, 229, 229, 0.5)',
        height: theme.headerHeight,
        color: 'rgba(0, 0, 0, 0.45)',
        display: 'flex',
        justifyContent: 'flex-start'
    },
    footerItem: {
        alignItems: 'center',
        margin: 'auto 20px',
        justifyContent: 'center'
    }
});

export default styles;
