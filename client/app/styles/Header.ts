const styles = theme => ({
    header: {
        right: 0,
        top: 0,
        zIndex: 10,
        background: theme.headerColor,
        position: 'fixed',
        lineHeight: `${theme.headerHeight}px`,
        transition: '0.2s',
        padding: 0,
        left: 0,
        boxShadow: '0 2px 8px rgba(229, 229, 229, 0.5)',
        display: 'flex',
        justifyContent: 'flex-start'
    },
    burgerButton: {
        fontSize: theme.fontSize,
        cursor: 'pointer',
        transition: 'color 0.3s',
        '&:hover': {
            color: theme.hoverColor
        },
        lineHeight: `${theme.headerHeight}px`,
        padding: '0 24px'
    },
    rightNav: {
        border: 'none',
        marginLeft: 'auto',
        display: 'flex',
        top: 0,
        background: theme.headerColor,
        lineHeight: `${theme.headerHeight}px`
    },
    rightNavItem: {
        border: 'none',
        lineHeight: `${theme.headerHeight}px`
    },
    logo: {
        height: 40,
        //background: 'rgba(255,255,255,0.2)',
        margin: 16
    },
    sidebar: {
        top: 0,
        left: 0,
        overflow: 'auto',
        height: '100vh',
        transition: '0.2s',
        position: 'fixed'
    },
    clearButton: {
        textAlign: 'center',
        height: '48px',
        lineHeight: '48px',
        cursor: 'pointer'
    },
    notificationIcon: {
        display: 'inline-block',
        height: '100%',
        padding: '0 12px',
        cursor: 'pointer',
        transition: 'all 0.3s',
        '&:hover': {
            background: theme.hoverColor
        }
    },
    iconButton: {
        width: '48px',
        height: '48px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: '24px',
        cursor: 'pointer'
    }
});

export default styles;
