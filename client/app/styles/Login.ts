const styles = theme => ({
    submit: {
        marginTop: theme.spacing,
        float: 'right',
    },
    signUpLink: {
        float: 'left',
        marginTop: theme.spacing,
    },
    forgotPasswordLink: {
        margin: theme.spacing,
    },
    logo: {
        //background: 'url(https://gw.alipayobjects.com/zos/rmsportal/sDUzcQUsFXjBgcHNCuiv.svg) no-repeat center',
        display: 'inline-block',
        width: 150,
        height: theme.headerHeight,
    },
});

export default styles;
