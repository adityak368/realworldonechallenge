const styles = theme => ({
    content: {
        minHeight: '100vh',
        flex: 1,
        transition: '0.2s',
        marginTop: `${theme.headerHeight + 8}px`,
        margin: 0,
        padding: 0
    },
    breadcrumbs: {
        margin: '10px 0px'
    },
    footer: {
        bottom: 0,
        marginTop: 16,
        width: '100%'
    }
});

export default styles;
