const styles = theme => ({
    submit: {
        marginTop: theme.spacing,
        float: 'right'
    },
    icon: {
        margin: theme.spacing
    },
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    profileItem: {
        width: '100%'
    },
    contact: { flex: 1 }
});

export default styles;
