const styles = theme => ({
    loginLink: {
        float: 'left',
        marginTop: theme.spacing * 2,
    },
    submit: {
        marginTop: theme.spacing,
        float: 'right',
    },
    container: {
        height: '100vh',
    },
    logo: {
        //background: 'url(https://gw.alipayobjects.com/zos/rmsportal/sDUzcQUsFXjBgcHNCuiv.svg) no-repeat center',
        display: 'inline-block',
        width: 150,
        height: theme.headerHeight,
    },
});

export default styles;
