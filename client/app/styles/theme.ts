const AppTheme = {
    spacing: 8,
    headerColor: '#FDFDFD',
    headerHeight: 64,
    name: 'dark',
    sideBarExpandedWidth: 200,
    sideBarCollapsedWidth: 80,
    fontSize: 18,
    hoverColor: '#1890ff'
};
export default AppTheme;
