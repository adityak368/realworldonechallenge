/**
 * Base webpack config used across other specific configs
 */

import path from 'path';
import webpack from 'webpack';
import { dependencies, productName } from '../package.json';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';

export default {

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'awesome-typescript-loader',
                options: {
                    cacheDirectory: true,
                },
            },
        ],
    },

    /**
     * Determine the array of extensions that should be used to resolve modules.
     */
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
        alias: {
            app: path.resolve(__dirname, '..', 'app'),
            styles: path.resolve(__dirname, '..', 'app', 'styles'),
            lib: path.resolve(__dirname, '..', 'app', 'lib'),
            res: path.resolve(__dirname, '..', 'app', 'res'),
            components: path.resolve(__dirname, '..', 'app', 'components'),
            pages: path.resolve(__dirname, '..', 'app', 'pages'),
            data: path.resolve(__dirname, '..', 'app', 'data'),
            containers: path.resolve(__dirname, '..', 'app', 'containers'),
            common: path.resolve(__dirname, '..', 'app', 'common'),
            routing: path.resolve(__dirname, '..', 'app', 'routing'),
            layouts: path.resolve(__dirname, '..', 'app', 'layouts'),
            contexts: path.resolve(__dirname, '..', 'app', 'contexts'),
            moment: `moment/moment.js`,
        },
    },

    plugins: [
        new webpack.EnvironmentPlugin({
            NODE_ENV: 'production',
        }),

        new webpack.NamedModulesPlugin(),
        //new CleanWebpackPlugin(['dist/**/*']),
        //new CopyWebpackPlugin([{ from: RES_DIR, to: 'dist' }]),
        // new HtmlWebpackPlugin({
        //     template: path.resolve(APP_DIR, 'index.html'),
        //     title: productName,
        //     publicPath: '/',
        //     //filename: path.join(DIST_DIR, 'index.html'),
        // }),

        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/),

    ],
};
