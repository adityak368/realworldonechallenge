import path from 'path';
import webpack from 'webpack';
import merge from 'webpack-merge';
import baseConfig from './webpack.config.base';
import CopyWebpackPlugin from 'copy-webpack-plugin';

const DIST_DIR = path.resolve(__dirname, '..', 'dist');
const APP_DIR = path.resolve(__dirname, '..', 'app');
const SERVER_DIR = path.resolve(
    __dirname,
    '..',
    '..',
    'server',
    'web',
    'public'
);

const port = process.env.PORT || 80;

export default merge.smart(baseConfig, {
    devtool: 'inline-source-map',

    mode: 'development',

    target: 'web',

    entry: [
        //'react-hot-loader/patch',
        `webpack-dev-server/client?http://localhost`,
        'webpack/hot/dev-server',
        'babel-polyfill',
        path.join(APP_DIR, 'index.tsx')
    ],

    output: {
        path: APP_DIR,
        filename: 'bundle.js',
        publicPath: '/assets'
    },

    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'style-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            modules: true,
                            localIdentName: '[name]_[local]_[hash:base64:10]',
                            importLoaders: 1
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true
                        }
                    }
                ]
            },
            {
                test: /\.less$/,
                use: [
                    {
                        loader: 'style-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'less-loader',
                        options: {
                            sourceMap: true,
                            javascriptEnabled: true
                            //modifyVars: themeVariables,
                        }
                    }
                ]
            },
            // WOFF Font
            {
                test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        mimetype: 'application/font-woff'
                    }
                }
            },
            // WOFF2 Font
            {
                test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        mimetype: 'application/font-woff'
                    }
                }
            },
            // TTF Font
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        mimetype: 'application/octet-stream'
                    }
                }
            },
            // EOT Font
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                use: 'file-loader'
            },
            // SVG Font
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        mimetype: 'image/svg+xml'
                    }
                }
            },
            // Common Image Formats
            {
                test: /\.(?:ico|gif|png|jpg|jpeg|webp)$/,
                use: 'url-loader'
            }
        ]
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin({
            multiStep: true
        }),

        new webpack.NoEmitOnErrorsPlugin(),

        new webpack.EnvironmentPlugin({
            NODE_ENV: 'development'
        }),
        new CopyWebpackPlugin([
            { from: DIST_DIR, to: path.resolve(SERVER_DIR, 'assets') },
            {
                from: path.resolve(APP_DIR, 'index.html'),
                to: path.resolve(SERVER_DIR)
            },
            {
                from: path.resolve(APP_DIR, 'logo.png'),
                to: path.resolve(SERVER_DIR)
            },
            {
                from: path.resolve(APP_DIR, 'favicon.png'),
                to: path.resolve(SERVER_DIR)
            }
        ]),
        new webpack.LoaderOptionsPlugin({
            debug: true
        })
    ],

    devServer: {
        contentBase: APP_DIR,
        port: port,
        hot: true,
        historyApiFallback: true
        // proxy: {
        //     "*": "http://localhost:3000/",
        // },
        //writeToDisk: true,
    }
});
