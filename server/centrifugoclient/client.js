const logger = require('pino')();

const { Request } = require('../lib/net/request');

class CentrifugoClient {
  constructor() {
    this.publish = this.publish.bind(this);
    this.presence = this.presence.bind(this);
    this.sendCommand = this.sendCommand.bind(this);
  }

  publish(channel, data) {
    return this.sendCommand({
      method: 'publish',
      params: {
        channel,
        data
      }
    });
  }

  presence(channel) {
    return this.sendCommand({
      method: 'presence_stats',
      params: {
        channel
      }
    });
  }

  sendCommand(command) {
    const req = new Request();
    return req.post(command);
  }
}

const client = new CentrifugoClient();
module.exports = client;
