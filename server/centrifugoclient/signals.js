module.exports = {
  BrokerSignals: {
    RequestStream: 'RequestStream',
    ChatMessage: 'ChatMessage',
    Notification: 'Notification'
  },
  Namespaces: {
    Users: 'user'
  }
};
