const GenerateMessage = (brokerSignal, data) => ({
    brokerSignal,
    ...data,
});

module.exports = {
    GenerateMessage,
};
