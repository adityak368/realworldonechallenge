const mongoose = require('mongoose');
const config = require('../config').mongo;

const connect = () => {
  mongoose.Promise = global.Promise;
  mongoose.connect(
    `mongodb://${config.USERNAME}:${config.PASSWORD}@${config.HOST_NAME}:${config.PORT}/${config.DATABASE_NAME}`,
    {
      keepAlive: true,
      reconnectTries: Number.MAX_VALUE,
      useMongoClient: true
    }
  );
};

module.exports = {
  connect
};
