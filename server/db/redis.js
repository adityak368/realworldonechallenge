const Redis = require('ioredis');
const config = require('../config').redis;

let pubRedis = null;
let subRedis = null;

const connect = () => {
    Redis.Promise = global.Promise;
    pubRedis = new Redis({
        port: config.PORT,
        host: config.HOST_NAME,
        db: config.DATABASE_NAME,
    });
    pubRedis.on('ready', () => {
        pubRedis.config('SET', 'notify-keyspace-events', 'Ex');
    });
    subRedis = new Redis({
        port: config.PORT,
        host: config.HOST_NAME,
    });
    subRedis.subscribe(`__keyevent@${config.DATABASE_NAME}__:expired`);
};

const getPubRedis = () => pubRedis;
const getSubRedis = () => subRedis;

module.exports = {
    connect,
    getPubRedis,
    getSubRedis,
};
