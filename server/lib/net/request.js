const request = require('superagent');
const { socketApiKey, socketUrl } = require('../../config');

class Request {

    constructor() {
        this.post = this.post.bind(this);
    }

    post(data) {
        const headers = this.generateHeaders();
        return new Promise((resolve, reject) => {
            request
                .post(socketUrl)
                .set(headers)
                .send(data)
                .end((error, res) => (error ? reject(error) : resolve(res.body)));
        });
    }

    generateHeaders() {
        return { 
            'Content-Type': 'application/json', 
            Authorization: `apikey ${socketApiKey}` 
        };
    }

};

const HandleError = err => {
    console.log(err);
    return err.response.body.errors.join('\n');
};

module.exports = {
    Request,
    HandleError,
};
