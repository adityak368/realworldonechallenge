const config = require('../config');
const AYLIENTextAPI = require('aylien_textapi');

const textapi = new AYLIENTextAPI({
  application_id: config.textAnalysisAppID,
  application_key: config.textAnalysisApiKey
});

const getSentiment = msg => {
  return new Promise((resolve, reject) => {
    textapi.sentiment(
      {
        text: msg
      },
      function(error, response) {
        if (error === null) {
          resolve(response);
          console.log(response);
        }
        reject(error);
      }
    );
  });
};

const getEmojiForSentiment = sentiment => {
  switch (sentiment) {
    case 'positive':
      return '🙂';
    case 'negative':
      return '🙁';
    case 'neutral':
      return '😐';
    default:
      return '';
  }
};
module.exports = {
  getSentiment,
  getEmojiForSentiment
};
