const { VerifyJwtToken, HandleError } = require('../utils');

const VerifyJwtMw = (req, res, next) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.split(' ')[0] !== 'Bearer'
  ) {
    return res.status(403).json(HandleError('No credentials sent!'));
  }

  const token = req.headers.authorization.split(' ')[1];

  VerifyJwtToken(token)
    .then(decodedToken => {
      req.user = decodedToken;
      next();
    })
    .catch(err =>
      res.status(403).json(HandleError('Invalid auth token provided.'))
    );
};

module.exports = {
  VerifyJwtMw
};
