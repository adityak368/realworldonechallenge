const { validationResult } = require('express-validator/check');

const errorFormatter = ({ location, msg, param, value, nestedErrors }) =>
  `${param}: ${msg}`;

exports.CheckErrors = (req, res, next) => {
  const errors = validationResult(req).formatWith(errorFormatter);
  if (!errors.isEmpty()) {
    return res.status(422).json({ message: errors.array().join('\n') });
  }
  next();
};
