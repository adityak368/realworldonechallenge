const mongoose = require('mongoose');

const { Schema } = mongoose;
const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');
const { secret } = require('../config');

const userSchema = new Schema(
  {
    email: {
      type: String,
      lowercase: true,
      unique: true,
      required: [true, 'Email is required'],
      match: [/\S+@\S+\.\S+/, 'Email is invalid'],
      index: true
    },
    username: {
      type: String,
      lowercase: true,
      unique: true,
      required: [true, 'Username is required'],
      index: true
    },
    name: String,
    password: String
  },
  { timestamps: true }
);

userSchema.methods.validPassword = function(password, cb) {
  bcrypt.compare(password, this.password, (err, isMatch) => {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

userSchema.pre('save', function(next) {
  const user = this;
  // Break out if the password hasn't changed
  if (!user.isModified('password')) {
    return next();
  }
  // password changed so we need to hash it
  bcrypt.genSalt(5, (err, salt) => {
    if (err) return next(err);

    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) return next(err);
      user.password = hash;
      next();
    });
  });
});

userSchema.post('save', function(error, doc, next) {
  console.log(error);
  if (error.name === 'MongoError' && error.code === 11000) {
    next(new Error('Email/Username already exists!'));
  } else if (error.name === 'ValidationError') {
    const errors = [];
    // go through all the errors...
    for (const errName in error.errors) {
      errors.push(error.errors[errName].message);
    }
    next(new Error(errors.join('\n')));
  } else {
    next(error);
  }
});

userSchema.methods.generateJWT = function() {
  return jwt.sign(
    {
      sub: this.username,
      info: {
        id: this._id,
        email: this.email,
        name: this.name,
        username: this.username
      }
    },
    secret,
    { expiresIn: 129600 }
  );
};

userSchema.methods.toAuthJSON = function() {
  return {
    authToken: this.generateJWT()
  };
};

userSchema.methods.GetCleanedUser = function() {
  return {
    id: this._id,
    email: this.email,
    name: this.name,
    username: this.username
  };
};

module.exports = mongoose.model('User', userSchema);
