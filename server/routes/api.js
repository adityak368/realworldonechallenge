const express = require('express');
const ApiValidator = require('../validation/api');
const client = require('../centrifugoclient/client');
const User = require('../models/user');
const { Namespaces } = require('../centrifugoclient/signals');
const { CheckErrors } = require('../middlewares/validation');
const { getEmojiForSentiment, getSentiment } = require('../lib/sentiment');

const api = express.Router();

api.route('/contacts').get(async (req, res) => {
  try {
    const users = await User.find({}).exec();
    if (users) {
      const requestPromises = [];
      users.forEach(user => {
        requestPromises.push(
          client.presence(`${Namespaces.Users}${user.username}`)
        );
      });
      const presenceData = await Promise.all(requestPromises);
      const presence = users.map((user, index) => ({
        ...user.GetCleanedUser(),
        isOnline: presenceData[index].result.num_clients > 0
      }));
      res.json(presence);
    } else {
      res.status(500).json(HandleError('Could Not Fetch Users'));
    }
  } catch (err) {
    console.log(err);
    throw { status: 500, message: err.message };
  }
});

api
  .route('/relay')
  .post(ApiValidator.validate('relay'), CheckErrors, async (req, res) => {
    const { target, data } = req.body;
    try {
      console.log(`${Namespaces.Users}${target}`);
      if (data.name === 'ChatMessage') {
        const sentiment = await getSentiment(data.data.msg.text);
        data.data.msg.text += getEmojiForSentiment(sentiment.polarity);
      }
      console.log(data);
      await client.publish(`${Namespaces.Users}${target}`, data);
      res.status(200).json({
        message: 'Sent Message to target'
      });
    } catch (err) {
      console.log(err);
      throw { status: 500, message: err.message };
    }
  });

module.exports = api;
