const express = require('express');

const auth = express.Router();
const User = require('../models/user');
const UserValidator = require('../validation/auth');
const { CheckErrors } = require('../middlewares/validation');
const { HandleError, HandleSucess } = require('../utils');

auth
  .route('/login')
  .post(UserValidator.validate('login'), CheckErrors, async (req, res) => {
    try {
      const user = await User.findOne({ email: req.body.email }).exec();
      if (user) {
        user.validPassword(req.body.password, (err, success) => {
          if (err) return res.status(500).json(HandleError(err));
          if (success) {
            return res.json(user.toAuthJSON());
          }
          res.status(401).json(HandleError('Invalid Email/Password'));
        });
      } else {
        res.status(401).json(HandleError('Invalid Email/Password'));
      }
    } catch (err) {
      throw { status: 500, message: err.message };
    }
  });

auth
  .route('/signup')
  .post(UserValidator.validate('signup'), CheckErrors, async (req, res) => {
    const user = new User(req.body);
    try {
      user.save(err => {
        if (err) return res.status(500).json(HandleError(err));
        res.status(201).json(HandleSucess('Created new user'));
      });
    } catch (err) {
      throw { status: 500, message: err.message };
    }
  });

module.exports = auth;
