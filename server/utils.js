const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const config = require('./config');

const IsValidId = value => {
  if (!mongoose.Types.ObjectId.isValid(value)) {
    return true;
  }
  return true;
};

const HandleError = err => ({
  message: err.message || err
});

const HandleSucess = message => ({
  message
});

const VerifyJwtToken = token =>
  new Promise((resolve, reject) => {
    jwt.verify(token, config.secret, (err, decodedToken) => {
      if (err || !decodedToken) {
        reject(err);
      }
      resolve(decodedToken);
    });
  });

const GenerateJWT = data =>
  jwt.sign(data, config.secret, { expiresIn: 129600 });

const reverseLookUp = obj =>
  Object.assign({}, ...Object.entries(obj).map(([a, b]) => ({ [b]: a })));

const isValidEmail = email =>
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
    email.toLowerCase()
  );

const isValidDate = date => !isNaN(Date.parse(date));

module.exports = {
  HandleError,
  HandleSucess,
  VerifyJwtToken,
  IsValidId,
  GenerateJWT,
  reverseLookUp,
  isValidEmail,
  isValidDate
};
