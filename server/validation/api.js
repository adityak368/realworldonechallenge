const { body } = require('express-validator/check');

exports.validate = method => {
  switch (method) {
    case 'relay':
      return [
        body('target', 'Invalid Target').exists(),
        body('data', 'Invalid data to relay').exists()
      ];
    default:
      return [];
  }
};
