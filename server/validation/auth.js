const { body } = require('express-validator/check');

exports.validate = method => {
  switch (method) {
    case 'signup':
      return [
        body('email', 'Invalid email')
          .exists()
          .isEmail(),
        body('password')
          .optional()
          .isAlphanumeric(),
        body('name').optional(),
        body('username', 'Invalid Username')
          .exists()
          .isAlphanumeric()
      ];
    case 'login':
      return [
        body('email', 'Invalid email')
          .exists()
          .isEmail(),
        body('password', 'Invalid password').exists(),
        body('remember')
          .optional()
          .isBoolean()
      ];
    default:
      return [];
  }
};
